using System.Linq;
using BarangaySystem.Data;
using BarangaySystem.Models.APIServices;
using BarangaySystem.Models.FacilityModels;
using BarangaySystem.Models.IdentityModels;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BarangaySystem.Controllers
{
    public class FacilityController : Controller
    {    
        public readonly UserManager<ApplicationUser> _userManager;

        public FacilityController(UserManager<ApplicationUser> userManager){

            _userManager = userManager;
        }
            
        private string LoggedUser{
            get{
                return _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name).Fullname;
            }
        }
        public IActionResult ViewFacility() => View();
        public IActionResult NewFacility() => View();
        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult SaveNewFacility(FacilityModel facilityModel)
        {
            string message = string.Empty;
            if (!ModelState.IsValid)
            {
                foreach (var err in ModelState.Values)
                {
                    foreach (var error in err.Errors)
                    {
                        message = "Oppss.!! Error!";
                        TempData["Error"] = message;
                        ModelState.AddModelError(string.Empty, error.ErrorMessage);
                    }
                }
                return RedirectToAction(nameof(FacilityController.NewFacility), facilityModel);
            }
            var api = new APIFacility();
            bool flag = api.Add(facilityModel: facilityModel, addedBy: LoggedUser, message: out message);
            if(!flag)
            {
                ModelState.AddModelError(string.Empty, message);
                return View("NewFacility", facilityModel);
            }
            message = "Facility has been Successfully Added";
            TempData["Success"] = message;
            return RedirectToAction(nameof(FacilityController.NewFacility));
        }

        [HttpGet("[controller]/EditFacility/{key}")]
        public IActionResult EditFacility(string key)
        {
            if (!key.IsValidKey())
            {
                return NotFound();
            }
            var api = new APIFacility();
            string id = key.DecryptString();
            return View(api.GetFacility(int.Parse(id)));
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult UpdateFacility(FacilityModel facilityModel)
        {
            if (!ModelState.IsValid)
            {
                foreach (var err in ModelState.Values)
                {
                    foreach (var error in err.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.ErrorMessage);
                    }
                }
                return NotFound();
            }

            if(!facilityModel.Id.ToString().Validate(facilityModel.Key))
            {
                return NotFound();
            }

            var api = new APIFacility();
            string message = string.Empty;
            bool flag = api.Update(facilityModel: facilityModel, addedBy: LoggedUser, message: out message);
            if(!flag)
            {
                ModelState.AddModelError(string.Empty, message);
                TempData["Error"] = message;
                return RedirectToAction(nameof(FacilityController.EditFacility),facilityModel);
            }
            TempData["Success"] = message;
            return RedirectToAction(nameof(FacilityController.ViewFacility));
        }

        [HttpGet]
        public JsonResult Remove(string key)
        {
            
            var api = new APIFacility();
            string message = string.Empty;
            var flag = api.Delete(key.DecryptString().ValidateNumber(), addedBy:LoggedUser, message: out message);

            return Json(new{
                status = flag,
                message = message
            });
        }

        [HttpPost]
        public JsonResult GetFacilitiesData(){

            string search = Request.Form["search[value]"];
            int start = int.Parse(Request.Form["start"]),
                pageSize = int.Parse(Request.Form["length"]),
                count = 0;
            var db = new DbBarangayContext();
            var query = db.Facility;
            count = query.Count();
            var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);

            var api = new APIFacility();
            var result = !search.IsEmpty() ? api.SearchFacilityData(search,start,pageSize, out count) : 
                                             api.GetFacilityData(start,pageSize, out count);
            return Json(new{
                data = result,
                draw = Request.Form["draw"],
                recordsTotal = count,
                recordsFiltered = count,
            });
        }
        
    }
}