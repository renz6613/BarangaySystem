using System;
using System.Linq;
using BarangaySystem.Data;
using BarangaySystem.Models.APIServices;
using BarangaySystem.Models.BlotterModels;
using BarangaySystem.Models.IdentityModels;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BarangaySystem.Controllers
{
    [Authorize]
    public class BlotterController : Controller
    {
        public readonly UserManager<ApplicationUser> _userManager;

        public BlotterController(UserManager<ApplicationUser> userManager){

            _userManager = userManager;
        }
            
        private string LoggedUser{
            get{
                return _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name).Fullname;
            }
        }
        public IActionResult ViewBlotter() => View();
        public IActionResult NewBlotter() => View();

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult SaveNewBlotter(BlotterModel blotterModel)
        {
            string message = string.Empty;
            if (!ModelState.IsValid)
            {
                foreach (var err in ModelState.Values)
                {
                    foreach (var error in err.Errors)
                    {
                        message = "Oppss.!! Error!";
                        TempData["Error"] = message;
                        ModelState.AddModelError(string.Empty, error.ErrorMessage);
                    }
                }
                return RedirectToAction(nameof(BlotterController.NewBlotter), blotterModel);
            }
            var api = new APIBlotter();
            bool flag = api.Add(blotterModel: blotterModel, addedBy: LoggedUser, message: out message);
            if(!flag)
            {
                ModelState.AddModelError(string.Empty, message);
                return View("NewBlotter", blotterModel);
            }
            message = "Person Data has been Successfully Added";
            TempData["Success"] = message;
            return RedirectToAction(nameof(BlotterController.NewBlotter));
        }

        [HttpGet("[controller]/EditBlotter/{key}")]
        public IActionResult EditBlotter(string key)
        {
            if (!key.IsValidKey())
            {
                return NotFound();
            }
            var api = new APIBlotter();
            string id = key.DecryptString();
            return View(api.GetBlotter(int.Parse(id)));
        }

        public IActionResult ViewDesc(string key)
        {
            if (!key.IsValidKey())
            {
                return NotFound();
            }
            var api = new APIBlotter();
            string id = key.DecryptString();
            return View(api.GetDesc(int.Parse(id)));
        }

        
        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult UpdateBlotter(BlotterModel blotterModel)
        {
            if (!ModelState.IsValid)
            {
                foreach (var err in ModelState.Values)
                {
                    foreach (var error in err.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.ErrorMessage);
                    }
                }
                return View(nameof(EditBlotter), blotterModel);
            }

            if(!blotterModel.Id.ToString().Validate(blotterModel.Key))
            {
                return View(nameof(EditBlotter), blotterModel);
            }

            var api = new APIBlotter();
            string message = string.Empty;
            bool flag = api.Update(blotterModel: blotterModel, addedBy:LoggedUser, message: out message);
            if(!flag)
            {
                ModelState.AddModelError(string.Empty, message);
                return View(nameof(EditBlotter), blotterModel);
            }
            TempData["Success!"] = message;
            return RedirectToAction(nameof(BlotterController.ViewBlotter));
        }


        [HttpPost]
        public JsonResult GetBlotterData(){

            string search = Request.Form["search[value]"];
            int start = int.Parse(Request.Form["start"]),
                pageSize = int.Parse(Request.Form["length"]),
                count = 0;
            var db = new DbBarangayContext();
            var query = db.Blotter;
            count = query.Count();
            var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);

            var api = new APIBlotter();
            var result = !search.IsEmpty() ? api.SearchBlotterData(search,start,pageSize, out count) : 
                                             api.GetBlotterData(start,pageSize, out count);
            return Json(new{
                data = result,
                draw = Request.Form["draw"],
                recordsTotal = count,
                recordsFiltered = count,
            });
        }

        [HttpGet]
        public JsonResult Remove(string key)
        {
            var api = new APIBlotter();
            string message = string.Empty;
            var flag = api.Delete(key.DecryptString().ValidateNumber(), addedBy:LoggedUser, message: out message);

            return Json(new{
                status = flag,
                message = message
            });
        }
        
    }
}