using System.Collections.Generic;
using System.Linq;
using BarangaySystem.Data;
using BarangaySystem.Models.APIServices;
using BarangaySystem.Models.IdentityModels;
using BarangaySystem.Models.ItemModels;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BarangaySystem.Controllers
{
    [Authorize]
    public class ItemController : Controller
    {
        
        public readonly UserManager<ApplicationUser> _userManager;

        public ItemController(UserManager<ApplicationUser> userManager){

            _userManager = userManager;
        }
        
        private string LoggedUser{
            get{
                return _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name).Fullname;
            }
        }
        public IActionResult ViewItem() => View();
        public IActionResult NewItem() => View();

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult SaveNewItem(ItemModel itemModel)
        {
            string message = string.Empty;
            if (!ModelState.IsValid)
            {
                foreach (var err in ModelState.Values)
                {
                    foreach (var error in err.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.ErrorMessage);
                    }
                }
                return RedirectToAction(nameof(ItemController.NewItem), itemModel);
            }
            var api = new APIItems();
            bool flag = api.Add(model: itemModel, addedBy: LoggedUser, message: out message);
            if(!flag)
            {
                ModelState.AddModelError(string.Empty, message);
                return View("NewItem", itemModel);
            }
            message = "Successfully Added";
            TempData["Success"] = message;
            return RedirectToAction(nameof(ItemController.NewItem));
            // return Json(new{
            //     TypeOfItem = itemModel.TypeOfItem,
            //         Name = itemModel.Name,
            //         Brand = itemModel.Brand,
            //         AddedBy = LoggedUser
            // });
        }

        [HttpGet("[controller]/EditItem/{Key}")]
        public IActionResult EditItem(string Key)
        {
            if (!Key.IsValidKey())
            {
                return NotFound();
            }
            var api = new APIItems();
            string id = Key.DecryptString();
            return View(api.GetItem(int.Parse(id)));
        }

        [HttpPost]
        public IActionResult UpdateItem(ItemModel itemModel)
        {
            string message = string.Empty;
            if (!ModelState.IsValid)
            {
                foreach (var err in ModelState.Values)
                {
                    foreach (var error in err.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.ErrorMessage);
                    }
                }
                return View("EditItem",itemModel);
            }

            if(!itemModel.Id.ToString().Validate(itemModel.Key))
            {
                
                return View("EditItem",itemModel);
            }

            var api = new APIItems();
            bool flag = api.Update(itemModel: itemModel, addedBy: LoggedUser, message: out message);
            if(!flag)
            {
                ModelState.AddModelError(string.Empty, message);
                TempData["Error"] = message;
                return View("EditItem",itemModel);
            }
            TempData["Success"] = message;
            return RedirectToAction(nameof(ItemController.ViewItem));
            // return Json(new{
            //     Id = itemModel.Id,
            //     Key = itemModel.Key,
            //     Type = itemModel.TypeOfItem,
            //     Name = itemModel.Name,
            //     Brand = itemModel.Brand
            // });
        }

        [HttpPost]
        public JsonResult GetItemsData(){

            string search = Request.Form["search[value]"];
            int start = int.Parse(Request.Form["start"]),
                pageSize = int.Parse(Request.Form["length"]),
                count = 0;
            var db = new DbBarangayContext();
            var query = db.Items;
            count = query.Count();
            var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);

            var api = new APIItems();
            var result = !search.IsEmpty() ? api.SearchItemsData(search,start,pageSize, out count) : 
                                             api.GetItemData(start,pageSize, out count);
            return Json(new{
                data = result,
                draw = Request.Form["draw"],
                recordsTotal = count,
                recordsFiltered = count,
            });

            // var items = new List<object> ();
            // foreach (var em in rows) {
            //     items.Add (new {
            //         Id = em.Id,
            //         TypeOfItem = em.TypeofItem,
            //         Name = em.Name,
            //         Brand = em.Brand,
            //         Models = db.Models.Where(x => x.ItemId == em.Id).Count().ToString()
            //     });
                
            // }
            // return Json(new{
            //     data = items,
            //     draw = Request.Form["draw"],
            //     recordsTotal = count,
            //     recordsFiltered = count,
            //     });
        }

        [HttpGet]
        public JsonResult Remove(string key)
        {
            
            var api = new APIItems();
            string message = string.Empty;
            var flag = api.Delete(key.DecryptString().ValidateNumber(), out message);

            return Json(new{
                status = flag,
                message = message
            });
        }
        
    }
}