using System;
using System.Linq;
using System.Threading.Tasks;
using BarangaySystem.Models.APIServices;
using BarangaySystem.Models.IdentityModels;
using BarangaySystem.Models.PermitModels;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BarangaySystem.Controllers
{
    public class BusinessController : Controller
    {
        public readonly UserManager<ApplicationUser> _userManager;

        public BusinessController(UserManager<ApplicationUser> userManager){

            _userManager = userManager;
        }
        
        private string LoggedUser{
            get{
                return _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name).Fullname;
            }
        }
        public IActionResult NewBusiness() => View();

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> NewBusiness(BusinessModel businessModel)
        {
            var xb = new System.Text.StringBuilder();
            if(!ModelState.IsValid){
                foreach (var err in ModelState.Values)
                {
                    foreach (var error in err.Errors)
                    {
                        xb.Append($"{ error.ErrorMessage } { Environment.NewLine }");
                        ModelState.AddModelError(string.Empty, error.ErrorMessage );
                    }
                }
                return View(businessModel);
                // return Content(xb.ToString());
            }
            if(!businessModel.PermitFee1.IsEmpty() && !businessModel.Php1.HasValue){
                ModelState.AddModelError(string.Empty, "Oops!? Permet 2. amount must not empty!");
                return View(businessModel);
            }
            if(!businessModel.PermitFee2.IsEmpty() && !businessModel.Php2.HasValue){
                ModelState.AddModelError(string.Empty, "Oops!? Permet 3. amount must not empty!");
                return View(businessModel);
            }
            if(!businessModel.PermitFee3.IsEmpty() && !businessModel.Php3.HasValue){
                ModelState.AddModelError(string.Empty, "Oops!? Permet 3. amount must not empty!");
                return View(businessModel);
            }
            var api=  new APIBusiness();
            businessModel.ReleasedBy = LoggedUser;
            var result = await api.SavePermit(businessModel);
            if(result.status){
                TempData["Success"] = result.message;
                return RedirectToAction(nameof(NewBusiness));
            }
            TempData["Error"] = result.message;
            return View(businessModel);
            
            // return Content(Newtonsoft.Json.JsonConvert.SerializeObject(businessModel));
        }

        public IActionResult ViewBusiness() => View();

        [HttpPost]
        public async Task<JsonResult> GetBusiness()
        {
            int start = Request.Form["start"].ToString().ValidateNumber(),
                length = Request.Form["length"].ToString().ValidateNumber();

            string search = Request.Form["search[value]"];
            
            var api = new APIBusiness();
            var result = await api.GetBusiness(start, length, search);
            return Json(new{
                data = result.list,
                recordsFiltered = result.count,
                recordsTotal = result.count
            });
        }

        [HttpGet("[controller]/EditBusiness/{key}")]
        public async Task<IActionResult> EditBusiness(string key){
            if(!key.IsValidKey()){
                return NotFound();
            }
            var api = new APIBusiness();
            var row = await api.EditBusiness(key.DecryptString().ValidateNumber());
            return View(row);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBusiness(BusinessModel businessModel)
        {
            var xb = new System.Text.StringBuilder();
            if(!ModelState.IsValid){
                foreach (var err in ModelState.Values)
                {
                    foreach (var error in err.Errors)
                    {
                        xb.Append($"{ error.ErrorMessage } { Environment.NewLine }");
                        ModelState.AddModelError(string.Empty, error.ErrorMessage );
                    }
                }
                return View(businessModel);
                // return Content(xb.ToString());
            }
            if(!businessModel.PermitFee1.IsEmpty() && !businessModel.Php1.HasValue){
                ModelState.AddModelError(string.Empty, "Oops!? Permet 2. amount must not empty!");
                return View(businessModel);
            }
            if(!businessModel.PermitFee2.IsEmpty() && !businessModel.Php2.HasValue){
                ModelState.AddModelError(string.Empty, "Oops!? Permet 3. amount must not empty!");
                return View(businessModel);
            }
            if(!businessModel.PermitFee3.IsEmpty() && !businessModel.Php3.HasValue){
                ModelState.AddModelError(string.Empty, "Oops!? Permet 3. amount must not empty!");
                return View(businessModel);
            }
            var api=  new APIBusiness();
            businessModel.ReleasedBy = nameof(BarangaySystem);
            var result = await api.UpdatePermit(businessModel);
            if(result.status){
                TempData["Success"] = result.message;
                return View();
            }
            TempData["Error"] = result.message;
            return View(businessModel);
            
            // return Content(Newtonsoft.Json.JsonConvert.SerializeObject(businessModel));
        }

        
    }
}