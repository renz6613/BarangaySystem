
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BarangaySystem.Models.IdentityModels;
using BarangaySystem.Services;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BarangaySystem.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public readonly UserManager<ApplicationUser> _userManager;
        public readonly SignInManager<ApplicationUser> _signInManager;
        public readonly RoleManager<ApplicationRole> _roleManager;

        private readonly IEmailSender _emailSender;

        public AccountController(UserManager<ApplicationUser> userManager,
                              RoleManager<ApplicationRole> roleManager,
                                 SignInManager<ApplicationUser> signInManager,
                                IEmailSender emailSender)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _emailSender = emailSender;

        }

        // [AllowAnonymous]
        // public async Task<string> CreateRole(){
        //     var newRole = new ApplicationRole{
        //         Name = "User",
        //         Description = "Regular user",
        //         DateCreated = DateTime.Now
        //     };
        //     IdentityResult newIdentity = await _roleManager.CreateAsync(newRole);
        //     if(!newIdentity.Succeeded){
        //         var sb = new System.Text.StringBuilder();

        //         foreach(var error in newIdentity.Errors){
        //             sb.Append($"{error.Description} {Environment.NewLine}");
        //         }
        //         return sb.ToString();
        //     }
        //     return "Success";
        // }

        public async Task<IActionResult> Index()
        {
            var currentUser = _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            var role = await _userManager.GetRolesAsync(currentUser);

            var user = new UserProfileViewModel
            {
                Name = currentUser.Fullname,
                Email = currentUser.Email,
                ContactNumber = currentUser.PhoneNumber,
                RoleName = role.FirstOrDefault()
            };
            return View(user);
        }

        [HttpPost, ValidateAntiForgeryToken, Authorize(Roles = "Administrator")]
        public async Task<IActionResult> AddUser(UserProfileModel userProfileModel)
        {
            var backTOPage = View(nameof(NewUser), userProfileModel);
            var message = string.Empty;

            if(!ModelState.IsValid){
                TempData["Error"] = "Invalid Input";
                return backTOPage ;
            }

            var checkExistingUser = _userManager.Users.Where(x => 
                x.Email == userProfileModel.Email || 
                x.UserName == userProfileModel.Email || 
                x.PhoneNumber == userProfileModel.ContactNumber ||
                x.Fullname == userProfileModel.Name
            );
            if(checkExistingUser.Any()){
                TempData["Error"] = "Oppss..! This account is already exist";
                return backTOPage ;
            }

            var newUser = new ApplicationUser
            {
                //custom properties
                Fullname = userProfileModel.Name,
                ImageFilename = null,
                OldPassword = null,
                DateCreated = DateTime.Now,


                //Builtin Prop
                UserName = userProfileModel.Email,
                Email = userProfileModel.Email,
                EmailConfirmed =true,
                PhoneNumber = userProfileModel.ContactNumber,
                PhoneNumberConfirmed=true

                
            };
            
            IdentityResult userResult = await _userManager.CreateAsync(newUser,"123456");
            if(userResult.Succeeded){
                IdentityResult setUSerRoleResult = await _userManager.AddToRoleAsync(newUser, "Staff");
                if(setUSerRoleResult.Succeeded){
                    message = "Successfully Added.!";
                    TempData["Success"] = message;
                    return backTOPage;
                }
            return backTOPage;
            }
                TempData["Error"] = "Oppss..! Something wen't wrong";
                return View(backTOPage);    
        }

        public async Task<IActionResult> Edit()
        {
            var currentUser = _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            var role = await _userManager.GetRolesAsync(currentUser);
            ViewBag.Type = role.FirstOrDefault();

            var user = new UserProfileModel{
                Name = currentUser.Fullname,
                Email = currentUser.Email,
                ContactNumber = currentUser.PhoneNumber
            };
            return View(user);
        }
        
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(UserProfileModel userProfileModel)
        {
            if(!ModelState.IsValid){
                return NotFound();
            }
            var currentUser = _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            currentUser.Fullname = userProfileModel.Name;
            currentUser.Email = userProfileModel.Email;
            currentUser.PhoneNumber = userProfileModel.ContactNumber;

            var message = string.Empty;
            var validate = _userManager.Users.Where(u => 
                u.UserName != currentUser.UserName && 
                (u.Email == currentUser.Email || u.PhoneNumber == currentUser.PhoneNumber));
            var redirect = RedirectToAction(nameof(Edit), userProfileModel);
            if(validate.Any()){
                message = "Oppss..!! The Email or Contact number is already exist..!!";
                TempData["Error"] = message;
                return redirect;
            }
            IdentityResult updateREsult = await _userManager.UpdateAsync(currentUser);
            if(updateREsult.Succeeded){
                return RedirectToAction(nameof(Index));
            }
            return redirect;
        }

        public  IActionResult ChangePass()
        {
            var currentUser = _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            ViewBag.Type = User.Identity.Name;
            return View();
        }

        [AllowAnonymous]
        public async Task<IActionResult> Login( string returnUrl = null)
        {
            if(User.Identity.IsAuthenticated)
            {
                return RedirectToLocal(returnUrl);
            }

            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            ViewData["returnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout(){
            await _signInManager.SignOutAsync();
            return RedirectToAction(nameof(HomeController.Index),"Home");
        }

        [HttpPost, AllowAnonymous, ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModels userModel, string returnUrl = null){
            ViewData["ReturnUrl"] = returnUrl;
            var message = string.Empty;
            if(!ModelState.IsValid){
                ModelState.AddModelError(string.Empty, "Oops!? something wen't wrong!");
            }
            var user = new ApplicationUser{
                UserName = userModel.Username
            };
            
            var check = _userManager.Users.Where(x => x.UserName == userModel.Username);
            if(check.Any() && !check.FirstOrDefault().LockoutEnabled)
            {
                // message = "This Accunt is Disabled.! Contact your admininstrator.!";
                // TempData["Error"] = message;
                ModelState.AddModelError(string.Empty, "This Account is Disabled.! Contact your admininstrator.!");
                // return RedirectToAction(nameof(HomeController.Index),"Home");
                return View(userModel);
            }

            
            var loginResult = await _signInManager.PasswordSignInAsync(userModel.Username,userModel.Password,false,lockoutOnFailure:false);
            if(loginResult.Succeeded){
                return RedirectToAction(nameof(HomeController.Index),"Home");
            }
            //User.Identity.Name
            // message = "Invalid username or password!";
            // TempData["Error"] = message;
                ModelState.AddModelError(string.Empty, "Invalid username or password!");
            return View(userModel);
        }


        [Authorize(Roles = "Administrator")]
        public IActionResult NewUser()
        {
            return View();
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePass(ChangePassModel changePassModel)
        {
            if(!ModelState.IsValid){
                ModelState.AddModelError(string.Empty, "Something went wrong!");
                return View(changePassModel);
            }
            var message = string.Empty;
            string _oldPass = string.Empty;
            
            var user = await _userManager.GetUserAsync(User);
            if(string.IsNullOrEmpty(user.OldPassword))
            {
                _oldPass = user.PasswordHash;
            }
            else
            {
                var verifyHashed = _userManager.PasswordHasher.VerifyHashedPassword(user, user.OldPassword, changePassModel.NewPass);
                if(verifyHashed == PasswordVerificationResult.Success)
                 {
                    ModelState.AddModelError (string.Empty, "Oops!? You cannot use your previous password.");
                    return View(nameof(ChangePass));
                 }
            }
            
            _oldPass = user.PasswordHash;
            IdentityResult updateREsult = await _userManager.ChangePasswordAsync(user, changePassModel.CurrentPass, changePassModel.NewPass);
            if(!updateREsult.Succeeded){
                AddErrors(updateREsult);
                return View(nameof(changePassModel));
            }

            user.OldPassword = _oldPass;
            await _userManager.UpdateAsync(user);
            
            TempData["Message"] = "Password successfully changed.";
            return RedirectToAction(nameof(ChangePass));
        }
        [Authorize(Roles= "Administrator")]
        public IActionResult ViewAccount()
        {
            return View();
        }

        
        [HttpPost, Authorize(Roles = "Administrator")]
        public async Task<JsonResult> GetAccountsData() 
        {
            string search = Request.Form["search[value]"];
            int start = int.Parse(Request.Form["start"]),
                pageSize = int.Parse(Request.Form["length"]),
                count = 0;


            var query = _userManager.Users.Where(x => x.UserName != User.Identity.Name);
            
            count = query.Count();
            var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);


            var query2 = _userManager.Users.Where(x => x.Fullname.StartsWith(search) || x.Email.StartsWith(search) || x.UserName.StartsWith(search));
            var rows2 = query2.OrderByDescending(x => x.Id).Take(pageSize).Skip(start);
            
            var result = !search.IsEmpty() ? rows2 : rows;

            var account = new List<object> ();
            foreach (var em in result) {
                var role = await _userManager.GetRolesAsync(em);
                account.Add (new {
                    Id = em.Id.EncryptString(),
                    Name = em.Fullname,
                    EmailAddress = em.Email,
                    Status = em.LockoutEnabled,
                    ContactNumber = em.PhoneNumber,
                    RoleName = role.FirstOrDefault(),
                });
                
            }
            return Json(new{
                data = account,
                draw = Request.Form["draw"],
                recordsTotal = count,
                recordsFiltered = count,});
        }

        [HttpPost, Authorize(Roles = "Administrator")]
        public async Task<JsonResult> UpdateStatus(string id, bool status)
        {
            var user = await _userManager.FindByIdAsync(id.DecryptString());
            IdentityResult result = await _userManager.SetLockoutEnabledAsync(user, !status);
            // await userManager.SetLockoutEnabledAsync(applicationUser.Id, true)
            // await userManager.SetLockoutEndDateAsync(DateTime.Today.AddYears(10));
            return Json(new{
                status = result.Succeeded
            });
        }
        

        #region Helpers

        [AllowAnonymous]
        public IActionResult ForgotPassword() => View();

        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation() => View();
        
        [AllowAnonymous]
        public IActionResult ConfirmEmail() => View();
        
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation() => View();

        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            var model = new ResetPasswordViewModel { Code = code };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel resetPasswordViewModel)
        {
            var model = resetPasswordViewModel;

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.EmailAddress);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            AddErrors(result);
            return View(resetPasswordViewModel);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordModel forgotPasswordModel)
        {
            if(ModelState.IsValid)
            {
                var model = forgotPasswordModel;
                var user = await _userManager.FindByEmailAsync(forgotPasswordModel.EmailAddress);
                if(user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    TempData["EmailForgotPassword"] = model.EmailAddress;
                    return RedirectToAction(nameof(ForgotPasswordConfirmation));
                }

                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
                await _emailSender.SendEmailAsync(model.EmailAddress, callbackUrl);
                TempData["EmailForgotPassword"] = model.EmailAddress;
                return RedirectToAction(nameof(ForgotPasswordConfirmation));
            }
            return View();
        }


        #endregion


        
        #region Helpers
        
        private IActionResult RedirectToLocal(string returnUrl){
            if(Url.IsLocalUrl(returnUrl)){
                return Redirect(returnUrl);
            }
            return RedirectToAction(nameof(HomeController.Index),"Home");
        }

        private void AddErrors(IdentityResult result){
            foreach(var error in result.Errors){
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
        #endregion

    }
}