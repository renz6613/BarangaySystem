using BarangaySystem.Models.APIServices;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace BarangaySystem.Controllers
{
    public class PrintController : Controller
    {

        [HttpGet("[controller]/printBarangay/{Key}")]
        public IActionResult printBarangay(string Key)
        {
            if (!Key.IsValidKey())
            {
                return NotFound();
            }
            var api = new APIBarangay();
            string id = Key.DecryptString();
            return View(api.GetPrintBarangay(int.Parse(id)));
        }



        [HttpGet("[controller]/printBusiness/{Key}")]
        public IActionResult printBusiness(string Key)
        {
            if (!Key.IsValidKey())
            {
                return NotFound();
            }
            var api = new APIBusiness();
            string id = Key.DecryptString();
            return View(api.GetPrintBusiness(int.Parse(id)));
        }

        [HttpGet("[controller]/OR/{Key}")]
        public IActionResult OR(string Key)
        {
            if (!Key.IsValidKey())
            {
                return NotFound();
            }
            var api = new APIBusiness();
            string id = Key.DecryptString();
            return View(api.GetOR(int.Parse(id)));
        }
        
    }
}