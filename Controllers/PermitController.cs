using System;
using System.Linq;
using BarangaySystem.Data;
using BarangaySystem.Models.APIServices;
using BarangaySystem.Models.IdentityModels;
using BarangaySystem.Models.PermitModels;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BarangaySystem.Controllers
{
    [Authorize]
    public class PermitController : Controller
    {
        public readonly UserManager<ApplicationUser> _userManager;

        public PermitController(UserManager<ApplicationUser> userManager){

            _userManager = userManager;
        }
        
        private string LoggedUser{
            get{
                return _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name).Fullname;
            }
        }
        public IActionResult ViewBarangay()
        {
            return View();
        }
        public IActionResult NewBarangay() => View();

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult SaveNewBarangay(BarangayModel barangayModel)
        {
            string message = string.Empty;
            if (!ModelState.IsValid)
            {
                foreach (var err in ModelState.Values)
                {
                    foreach (var error in err.Errors)
                    {
                        message = "Something wen't wrong";
                        TempData["Error"] = message;
                        ModelState.AddModelError(string.Empty, error.ErrorMessage);
                    }
                }
                return RedirectToAction(nameof(PermitController.NewBarangay), barangayModel);
            }
            var api = new APIBarangay();
            bool flag = api.Add(barangayModel: barangayModel, ReleasedBy: LoggedUser, message: out message);
            if(!flag)
            {
                ModelState.AddModelError(string.Empty, message);
                return View("NewBarangay", barangayModel);
            }
            message = "Barangay Permit Record has been Successfully Added";
            TempData["Success"] = message;
            return RedirectToAction(nameof(PermitController.NewBarangay));
        }

        [HttpPost]
        public JsonResult GetBarangayData(){

            string search = Request.Form["search[value]"];
            int start = int.Parse(Request.Form["start"]),
                pageSize = int.Parse(Request.Form["length"]),
                count = 0;
            var db = new DbBarangayContext();
            var query = db.Blotter;
            count = query.Count();
            var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);

            var api = new APIBarangay();
            var result = !search.IsEmpty() ? api.SearchBarangayData(search,start,pageSize, out count) : 
                                             api.GetBarangayDatas(start,pageSize, out count);
            return Json(new{
                data = result,
                draw = Request.Form["draw"],
                recordsTotal = count,
                recordsFiltered = count,
            });
        }

        public PartialViewResult GetAddress(long id)
        {
            var api = new APIBarangay();
            return PartialView("Partials/Controls/Address", api.GetAddressValues(id));
        }
    }
}