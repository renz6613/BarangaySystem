using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BarangaySystem.Data;
using BarangaySystem.Models.APIServices;
using BarangaySystem.Models.IdentityModels;
using BarangaySystem.Models.PersonModels;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BarangaySystem.Controllers
{
    [Authorize]
    public class PersonController : Controller
    {
        public readonly UserManager<ApplicationUser> _userManager;

        public PersonController(UserManager<ApplicationUser> userManager){

            _userManager = userManager;
        }
        
        private string LoggedUser{
            get{
                return _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name).Fullname;
            }
        }
        public IActionResult ViewPerson() => View();
        public IActionResult NewPerson() => View();

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult SaveNewPerson(PersonModel personModel)
        {
            string message = string.Empty;
            if (!ModelState.IsValid)
            {
                foreach (var err in ModelState.Values)
                {
                    foreach (var error in err.Errors)
                    {
                        message = "Oppss.!! Error!";
                        TempData["Error"] = message;
                        ModelState.AddModelError(string.Empty, error.ErrorMessage);
                    }
                }
                return RedirectToAction(nameof(PersonController.NewPerson), personModel);
            }
            var api = new APIPersons();
            bool flag = api.Add(personModel: personModel, addedBy: LoggedUser, message: out message);
            if(!flag)
            {
                ModelState.AddModelError(string.Empty, message);
                return View("NewPerson", personModel);
            }
            message = "Person Data has been Successfully Added";
            TempData["Success"] = message;
            return RedirectToAction(nameof(PersonController.NewPerson));
        }

        [HttpGet("[controller]/EditPerson/{key}")]
        public IActionResult EditPerson(string key)
        {
            if (!key.IsValidKey())
            {
                return NotFound();
            }
            var api = new APIPersons();
            string id = key.DecryptString();
            return View(api.GetPerson(int.Parse(id)));
        }

        public IActionResult ViewBorrowed()
        {
            return View();
        }

        public PartialViewResult GetModels(long id)
        {
            var api = new APIPersons();
            return PartialView("Partials/Controls/Models", api.GetModelValues(id));
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult UpdatePerson(PersonModel personModel)
        {
            if (!ModelState.IsValid)
            {
                foreach (var err in ModelState.Values)
                {
                    foreach (var error in err.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.ErrorMessage);
                    }
                }
                return NotFound();
            }

            if(!personModel.Id.ToString().Validate(personModel.Key))
            {
                return NotFound();
            }

            var api = new APIPersons();
            string message = string.Empty;
            bool flag = api.Update(personModel: personModel, addedBy: LoggedUser, message: out message);
            if(!flag)
            {
                ModelState.AddModelError(string.Empty, message);
                TempData["Error"] = message;
                return RedirectToAction(nameof(PersonController.EditPerson),personModel);
            }
            TempData["Success"] = message;
            return RedirectToAction(nameof(PersonController.ViewPerson));
        }


        [HttpPost]
        public JsonResult GetPersonsData(){

            string search = Request.Form["search[value]"];
            int start = int.Parse(Request.Form["start"]),
                pageSize = int.Parse(Request.Form["length"]),
                count = 0;
            var db = new DbBarangayContext();
            var query = db.Persons;
            count = query.Count();
            var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);

            var api = new APIPersons();
            var result = !search.IsEmpty() ? api.SearchPersonsData(search,start,pageSize, out count) : 
                                             api.GetPersonsData(start,pageSize, out count);
            return Json(new{
                data = result,
                draw = Request.Form["draw"],
                recordsTotal = count,
                recordsFiltered = count,
            });
        }

        [HttpPost]
        public JsonResult GetBorrowData(string key){

            string search = Request.Form["search[value]"];
            int start = int.Parse(Request.Form["start"]),
                pageSize = int.Parse(Request.Form["length"]),
                count = 0;
            var db = new DbBarangayContext();
            var query = db.BorrowItem;
            count = query.Count();
            var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);

            string id = key.DecryptString();

            var api = new APIBorrowItem();
            var result = !search.IsEmpty() ? api.SearchBorrowsData(search,start,pageSize, out count) : 
                                             api.GetBorrowsData(start,pageSize, out count);
            return Json(new{
                data = result,
                draw = Request.Form["draw"],
                recordsTotal = count,
                recordsFiltered = count,
            });
            
        }


        [HttpGet]
        public JsonResult Remove(string key)
        {
            
            var api = new APIPersons();
            string message = string.Empty;
            var flag = api.Delete(key.DecryptString().ValidateNumber(), addedBy: LoggedUser, message: out message);

            return Json(new{
                status = flag,
                message = message
            });
        }
    }
}