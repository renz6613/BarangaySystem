using System;
using System.Linq;
using System.Threading.Tasks;
using BarangaySystem.Data;
using BarangaySystem.Models.APIServices;
using BarangaySystem.Models.BorrowItemModels;
using BarangaySystem.Models.IdentityModels;
using BarangaySystem.Models.PersonModels;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BarangaySystem.Controllers
{
    [Authorize]
    public class BorrowItemController : Controller
    {
        public readonly UserManager<ApplicationUser> _userManager;

        public BorrowItemController(UserManager<ApplicationUser> userManager){

            _userManager = userManager;
        }
            
        private string LoggedUser{
            get{
                return _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name).Fullname;
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult SaveNewBorrow(BorrowItemModel borrowItemModel)
        {
            var api = new APIBorrowItem();
            string message = string.Empty;
            bool flag = api.AddBorrow(borrowItemModel: borrowItemModel, addedBy: LoggedUser, message: out message);
            if(!flag)
            {
                ModelState.AddModelError(string.Empty, message);
                return View("NewBorrow",borrowItemModel);
            }
            TempData["Success!"] = message;
            return View(nameof(BorrowItemController.NewBorrow));
        }
        


        /// <summary>
        /// Kani siya para mang hulam ang tao ug item
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>

        public IActionResult NewBorrow(){
            // var id = p.Id;
            // var api = new APIBorrowItem();
            return View();
        }

        [HttpPost]
        public JsonResult Release(string key)
        {
            
            var api = new APIPersons();
            string message = string.Empty;
            var flag = api.ItemRelease(key.DecryptString().ValidateNumber(), addedBy:LoggedUser, message: out message);

            return Json(new{
                status = flag,
                message = message
            });
        }

        [HttpPost]
        public JsonResult Return(string key)
        {
            
            var api = new APIPersons();
            string message = string.Empty;
            var flag = api.ItemReturn(key.DecryptString().ValidateNumber(), addedBy: LoggedUser, message: out message);

            return Json(new{
                status = flag,
                message = message
            });
        }

        // [HttpGet("[controller]/BorrowersSlip/{key}")]
        public IActionResult BorrowersSlip(string key){
            var db = new DbBarangayContext ();
            if (!key.IsValidKey())
            {
                return NotFound();
            }

            var api = new APIPersons();
            string id = key.DecryptString();
            string s = LoggedUser;
            return View(api.GetPersonBorrow(int.Parse(id),s));
        }

        // [HttpPost]
        // public async Task<JsonResult> GetBorrowed(string key)
        // {
        //     int start = Request.Form["start"].ToString().ValidateNumber(),
        //         length = Request.Form["length"].ToString().ValidateNumber();y

        //     string search = Request.Form["search[value]"];
            
        //     var api = new APIActivity();
        //     var result = await api.GetActivity(start, length, search);
        //     return Json(new{
        //         data = result.list,
        //         recordsFiltered = result.count,
        //         recordsTotal = result.count
        //     });
        // }

        [HttpPost]
        public JsonResult GetBorrowed(string key){

            int start = int.Parse(Request.Form["start"]),
                pageSize = int.Parse(Request.Form["length"]),
                count = 0;
            var db = new DbBarangayContext();
            var query = db.BorrowItem;
            count = query.Count();
            var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);

            string id = key.DecryptString();

            var api = new APIBorrowItem();
            var result = api.GetBorrowData(int.Parse(id),start,pageSize, out count);
            return Json(new{
                data = result,
                draw = Request.Form["draw"],
                recordsTotal = count,
                recordsFiltered = count,
            });
        }
    }
}