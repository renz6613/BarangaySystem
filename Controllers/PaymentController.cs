using System.Linq;
using System.Threading.Tasks;
using BarangaySystem.Data;
using BarangaySystem.Models.APIServices;
using BarangaySystem.Models.BPmodels;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace BarangaySystem.Controllers
{
    public class PaymentController : Controller
    {
        
        public IActionResult ViewPayment()
        {
            return View();
        }


        [HttpPost]
        public async Task<JsonResult> GetPayments()
        {
            int start = Request.Form["start"].ToString().ValidateNumber(),
                length = Request.Form["length"].ToString().ValidateNumber();

            string search = Request.Form["search[value]"];
            var api = new APIPayment();
            var result = await api.GetPayment(start, length, search);
            return Json(new{
                data = result.list,
                recordsFiltered = result.count,
                recordsTotal = result.count
            });
        }
        
        
    }
}