﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BarangaySystem.Models;
using BarangaySystem.Models.IdentityModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using BarangaySystem.Utilities;
using BarangaySystem.Models.APIServices;

namespace BarangaySystem.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> GetActivity()
        {
            int start = Request.Form["start"].ToString().ValidateNumber(),
                length = Request.Form["length"].ToString().ValidateNumber();

            string search = Request.Form["search[value]"];
            
            var api = new APIActivity();
            var result = await api.GetActivity(start, length, search);
            return Json(new{
                data = result.list,
                recordsFiltered = result.count,
                recordsTotal = result.count
            });
        }
    }
}
