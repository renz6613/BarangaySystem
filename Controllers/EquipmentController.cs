using System;
using System.Collections.Generic;
using System.Linq;
using BarangaySystem.Data;
using BarangaySystem.Models;
using BarangaySystem.Models.APIServices;
using BarangaySystem.Models.ItemModels;
using BarangaySystem.Models.ModelModels;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using BarangaySystem.Models.IdentityModels;

namespace BarangaySystem.Controllers
{
    [Authorize]
    public class EquipmentController : Controller
    {
        public readonly UserManager<ApplicationUser> _userManager;

        public EquipmentController(UserManager<ApplicationUser> userManager){

            _userManager = userManager;
        }
        
        private string LoggedUser{
            get{
                return _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name).Fullname;
            }
        }


        [Authorize]
        public IActionResult ViewEquipment()
        {
            return View();
        }
        public IActionResult Inventory()
        {
            return View();
        }


        [HttpGet("[controller]/NewItemModel/{key}")]
        public IActionResult NewItemModel(string key)
        {
            if (!key.IsValidKey())
            {
                return NotFound();
            }
            string id = key.DecryptString();
            var api = new APIModels();
            var itemModel = new ModelModel();
            itemModel.ItemId = int.Parse(id);
            itemModel.Key = key;
            itemModel.BrandName = api.GetBrandNameById(int.Parse(id));
            return View(itemModel);
        }

        

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult SaveNewModel(ModelModel modelModel)
        {
            if (!ModelState.IsValid)
            {
                foreach (var err in ModelState.Values)
                {
                    foreach (var error in err.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.ErrorMessage);
                    }
                }
                return View("NewItemModel", modelModel);
            }
            var api = new APIModels();
            string message = string.Empty;
            bool flag = api.Add(model: modelModel, LoggedUser: LoggedUser, message: out message);
            if(!flag)
            {
                ModelState.AddModelError(string.Empty, message);
                return View("NewItemModel", modelModel);
            }
            message = "Successfully Added";
            TempData["Success"] = message;
            return RedirectToAction(nameof(EquipmentController.ViewEquipment));
            // return Json(new{
            //     ItemId = modelModel.ItemId,
            //     Name = modelModel.Name,
            //     Price = modelModel.Price,
            //     AddedBy = LoggedUser,
            //     DateAdded = DateTime.Now
            // });
        }



        [HttpGet("[controller]/EditItemModel/{key}")]
        public IActionResult EditItemModel(string Key)
        {
            if (!Key.IsValidKey())
            {
                return NotFound();
            }
            var api = new APIModels();
            string id = Key.DecryptString();
            return View(api.GetModel(int.Parse(id)));
        }

        [HttpPost]
        public IActionResult UpdateItemModel(ModelModel modelModel)
        {
            if (!ModelState.IsValid)
            {
                foreach (var err in ModelState.Values)
                {
                    foreach (var error in err.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.ErrorMessage);
                    }
                }
                return View("EditItemModel", modelModel);
            }

            if(!modelModel.Id.ToString().Validate(modelModel.Key))
            {
                return View("EditItemModel", modelModel);
            }

            var api = new APIModels();
            string message = string.Empty;
            bool flag = api.Update(modelModel: modelModel, addedBy: LoggedUser, message: out message);
            if(!flag)
            {
                ModelState.AddModelError(string.Empty, message);
                TempData["Error"] = message;
                return View("EditItemModel", modelModel);
            }
            TempData["Success"] = message;
            return RedirectToAction(nameof(EquipmentController.ViewEquipment));
            // return Json(new{
            //     Id = itemModel.Id,
            //     Key = itemModel.Key,
            //     Type = itemModel.TypeOfItem,
            //     Name = itemModel.Name,
            //     Brand = itemModel.Brand
            // });
        }

        [HttpPost]
        public JsonResult GetEquipmentData(){

            string search = Request.Form["search[value]"];
            int start = int.Parse(Request.Form["start"]),
                pageSize = int.Parse(Request.Form["length"]),
                count = 0;
            var db = new DbBarangayContext();
            var query = db.Models;
            count = query.Count();
            var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);

            var api = new APIModels();
            var result = !search.IsEmpty() ? api.SearchEquipmentData(search,start,pageSize, out count) : 
                                             api.GetEquipmentData(start,pageSize, out count);
            return Json(new{
                data = result,
                draw = Request.Form["draw"],
                recordsTotal = count,
                recordsFiltered = count,
            });
        }
        
        [HttpPost]
        public JsonResult GetInventory(){

            string search = Request.Form["search[value]"];
            int start = int.Parse(Request.Form["start"]),
                pageSize = int.Parse(Request.Form["length"]),
                count = 0;
            var db = new DbBarangayContext();
            var query = db.Models;
            count = query.Count();
            var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);

            var api = new APIModels();
            var result = api.GetInventoryData(start,pageSize, out count);
            return Json(new{
                data = result,
                draw = Request.Form["draw"],
                recordsTotal = count,
                recordsFiltered = count,
            });
        }

        [HttpGet]
        public JsonResult Remove(string key)
        {
            
            var api = new APIModels();
            string message = string.Empty;
            var flag = api.Delete(key.DecryptString().ValidateNumber(), addedBy: LoggedUser, message: out message);

            return Json(new{
                status = flag,
                message = message
            });
        }
    }
}