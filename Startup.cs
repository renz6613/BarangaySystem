﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using BarangaySystem.Data;
using BarangaySystem.Models;
using BarangaySystem.Models.IdentityModels;
using Microsoft.AspNetCore.Authentication.Cookies;
using BarangaySystem.Services;
using Microsoft.Extensions.Logging;

namespace BarangaySystem
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        public string GetConnectionString{
            get{
                return Configuration.GetConnectionString("DefaultConnection");
            }
        }

        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            BarangaySystem.Data.DbBarangayContext.GetConnectionString = GetConnectionString;
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(this.GetConnectionString));

            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            
            services.Configure<IdentityOptions> (options => {
                // Password settings
                options.Password.RequireDigit = true;
                // options.Password.RequiredLength = 8;
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                //user settings
                options.User.RequireUniqueEmail = true;
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.AccessDeniedPath = "/Account/AccessDenied";
                options.Cookie.Name = "BarangayCookies";
                options.Cookie.HttpOnly = true; 
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30); 
                options.LoginPath = "/Account/Login";
                options.LogoutPath = "/Account/Logout";
                // ReturnUrlParameter requires `using Microsoft.AspNetCore.Authentication.Cookies;`
                options.SlidingExpiration = true;
            });

            services.AddOptions();
            services.Configure<EmailSettings> (Configuration.GetSection("EmailSettings"));
            services.AddTransient<IEmailSender, EmailSender>();
            
            services.AddAntiforgery(options =>
            {
                options.FormFieldName = "wtf";
                options.HeaderName = "X-XSRF-TOKEN";
            });

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseStatusCodePagesWithRedirects("/Error/index/{0}");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvcWithDefaultRoute();

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

        }
    }
}
