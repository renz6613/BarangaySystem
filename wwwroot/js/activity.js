$table = null;
$(document).ready(function() {
    var handleDataTableButtons = function() {
      if ($("#tblActivity").length) {
        $table = $("#tblActivity").DataTable({
            lengthMenu: [5, 10, 15],
            processing: true,
            serverSide: true,
            autoWidth: false,
          dom: "Bfrtip",
          targets: 'no-sort', 'bSort': false, 'order': [],
          ajax: {
              'url': '/Home/GetActivity',
              'type': 'POST',
              'dataType': 'JSON'
          },
          buttons: [
            {
              extend: "copy",
              className: "btn-sm"
            },
            {
              extend: "csv",
              className: "btn-sm"
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm",
            },
          ],
          columns:[
              {'data': 'activityDate'},
              {'data': 'description'}
          ],
          responsive: true
        });
      }
    };

    $(".dataTables_filter input").unbind().bind('keyup change', function (e) {
      if (e.keyCode == 13 || this.value == "") {
          $table.search(this.value)
                .draw();
      }
  });

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();

    TableManageButtons.init();
  });
