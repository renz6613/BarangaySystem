$table = null;
$(document).ready(function() {
    var handleDataTableButtons = function() {
      if ($("#tblItems").length) {
        $table = $("#tblItems").DataTable({
            lengthMenu: [5, 10, 15],
            processing: true,
            serverSide: true,
            autoWidth: false,
          dom: "Bfrtip",
          targets: 'no-sort', 'bSort': false, 'order': [],
          ajax: {
              'url': '/Item/GetItemsData',
              'type': 'POST',
              'dataType': 'JSON'
          },
          buttons: [
            {
              extend: "copy",
              className: "btn-sm",
              exportOptions: {
                columns: [ 0, 1,2,3,4,5 ]
              }
            },
            {
              extend: "csv",
              className: "btn-sm",
              exportOptions: {
                columns: [ 0, 1,2,3,4,5 ]
              }
            },
            {
              extend: "excel",
              className: "btn-sm",
              exportOptions: {
                columns: [ 0, 1,2,3,4,5 ]
              }
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm",
              exportOptions: {
                columns: [ 0, 1,2,3,4,5 ]
              }
            },
            {
              extend: "print",
              className: "btn-sm",
              exportOptions: {
                columns: [ 0, 1,2,3,4,5 ]
              }
            },
          ],
          columns:[
              {'data': 'name'},
              {'data': 'brand'},
              {'data': 'models'},
              {'data': 'addedBy'},
              {'data': 'dateAdded'},
              {'data': 'dateModified'},
              {
                'data': null,
                width: '10%',
                render : function(data, type, row){
                    var edit = '<a href="/Item/EditItem/'+ row.key +'" id="btnEdit" class="btn btn-xs btn-info  btn-block"><i class="fa fa-edit"></i> Edit</a>',
                        add = '<a href="/Equipment/NewItemModel/'+ row.key +'" id="btnNewItemModel" class="btn btn-xs btn-primary  btn-block"><i class="fa fa-plus"></i> Add Model</a>';
                    return edit.concat(add);
                }
            }
          ],
          responsive: true
        });
      }
    };

    $(".dataTables_filter input").unbind().bind('keyup change', function (e) {
      if (e.keyCode == 13 || this.value == "") {
          $table.search(this.value)
                .draw();
      }
  });

  $('#tblItems tbody').on('click', '#btnRemove', function(){
    var row = $table.row($(this).parents('tr')).data();
    swal({
        title: 'Delete '+ row.name +'->'+ row.brand,
        text: "Are you sure to remove this?",
        type: 'question',
        allowOutsideClick: false,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then((result) => {
        if (result.value) {
            $.ajax({
                url: '/Item/Remove',
                type: 'get',
                dataType: 'json',
                data: {id: row.id, key: row.key},
                success: function(d){
                    if(d.status){
                        $table.ajax.reload();
                        return false;
                    }
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: d.message
                    })
                }
            })
            console.log('Removed');
            return false;
        }
      })
});

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();

    TableManageButtons.init();
  });
