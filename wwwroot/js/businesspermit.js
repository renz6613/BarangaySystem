$(function(){
    $('.fee').keyup(function(){
        var total = "0.00";
        $('.fee').each(function(){
            var value = $(this).val()
            if(!isNaN(value)){
                var num = Number(value),
                    t = Number(total);
                t += num;
                total = t;
            }
        });
        $('#total').text(total.toFixed(2));
    });
})