$table = null;
$(document).ready(function() {
  var url = new URL(window.location.href);
  var key = url.searchParams.get('key');

    var handleDataTableButtons = function() {
      if ($("#tblBorrows").length) {
        $table = $("#tblBorrows").DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
          dom: "Brt",
          targets: 'no-sort', 'bSort': false, 'order': [],
          ajax: {
              'url': '/BorrowItem/GetBorrowed',
              'type': 'POST',
              'dataType': 'JSON',
              'data': function(d){
                d.key = key;
              }
          },
          buttons: [
          ],
          columns:[
              {'data': 'itemName'},
              {'data': 'itemQuantity'}
          ],
          responsive: true
        });
      }
    };
    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();

    TableManageButtons.init();
  });
