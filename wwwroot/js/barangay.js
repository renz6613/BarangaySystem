$table = null;
$(document).ready(function() {
    var handleDataTableButtons = function() {
      if ($("#tblBanrangay").length) {
        $table = $("#tblBanrangay").DataTable({
            lengthMenu: [5, 10, 15],
            processing: true,
            serverSide: true,
            autoWidth: false,
          dom: "Bfrtip",
          targets: 'no-sort', 'bSort': false, 'order': [],
          ajax: {
              'url': '/Permit/GetBarangayData',
              'type': 'POST',
              'dataType': 'JSON'
          },
          buttons: [
            {
              extend: "copy",
              className: "btn-sm",
              exportOptions: {
                columns: [ 0, 1,2,3,4,5,6]
              }
            },
            {
              extend: "csv",
              className: "btn-sm",
              exportOptions: {
                columns: [ 0, 1,2,3,4,5,6]
              }
            },
            {
              extend: "excel",
              className: "btn-sm",
              exportOptions: {
                columns: [ 0, 1,2,3,4,5,6]
              }
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm",
              title: "Barangay Permit Report",
              exportOptions: {
                columns: [ 0, 1,2,3,4,5,6]
              }
            },
          ],
          columns:[
              {'data': 'name'},
              {'data': 'address'},
              {'data': 'dateOfBirth'},
              {'data': 'purpose'},
              {'data': 'issuedDate'},
              {'data': 'releasedBy'},
              {'data': 'dateRequested'},
              {
                'data': null,
                'width': '1%',
                render : function(data, type, row){
                    return '<a href="/Print/printBarangay/'+ row.key +'" class="btn btn-xs btn-info  btn-block"><i class="fa fa-Print"></i> Print</a>'
                }
              },
          ],
          responsive: true
        });
      }
    };

    $(".dataTables_filter input").unbind().bind('keyup change', function (e) {
      if (e.keyCode == 13 || this.value == "") {
          $table.search(this.value)
                .draw();
      }
  });

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();

    TableManageButtons.init();
  });
