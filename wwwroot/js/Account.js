$table = null;
$(document).ready(function() {
    var handleDataTableButtons = function() {
      if ($("#tblAccount").length) {
        $table = $("#tblAccount").DataTable({
            lengthMenu: [5, 10, 15],
            processing: true,
            serverSide: true,
            autoWidth: false,
          dom: "Bfrtip",
          targets: 'no-sort', 'bSort': false, 'order': [],
          ajax: {
              'url': '/Account/GetAccountsData',
              'type': 'POST',
              'dataType': 'JSON'
          },
          buttons: [
            {
              extend: "copy",
              className: "btn-sm"
            },
            {
              extend: "csv",
              className: "btn-sm"
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm"
            },
          ],
          columns:[
              {'data': 'name'},
              {'data': 'emailAddress'},
              {'data': 'contactNumber'},
              {'data': 'roleName'},
              {
                'data': null,
                width: '1%',
                render : function(data, type, row){
                  var btnColor = row.status ? 'primary' : 'danger',
                      btnIcon = row.status ? 'unlock' : 'lock',
                      btnText = row.status ? 'Enabled' : 'Disabled',
                      icon = ('').concat('<i class="fa fa-',btnIcon,'"></i> ');
                  return ('').concat('<button id="btnActivation" class="btn btn-xs btn-',btnColor,' btn-block">',icon, btnText,'</button>')
                }
            }
          ],
          responsive: true
        });
      }
    };


    $(".dataTables_filter input").unbind().bind('keyup change', function (e) {
      if (e.keyCode == 13 || this.value == "") {
          $table.search(this.value)
                .draw();
      }
    });

    $('#tblAccount tbody').on('click','#btnActivation',function(){
      var row = $table.row($(this).parents('tr')).data();
      $.ajax({
          'url': '/Account/UpdateStatus',
          'type': 'POST',
          'dataType': 'JSON',
          'data': { id: row.id, status: row.status, name: row.name },
          beforeSend: function(){
              //
          },
          success: function(res){
              if(res.status){
                  $table.ajax.reload();
              }
          }
      })
      console.log(row);
    });

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();

    TableManageButtons.init();
  });
