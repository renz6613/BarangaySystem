$table = null;
$(document).ready(function() {
    var handleDataTableButtons = function() {
      if ($("#tblEquipment").length) {
        $table = $("#tblEquipment").DataTable({
            lengthMenu: [5, 10, 15],
            processing: true,
            serverSide: true,
            autoWidth: false,
          dom: "Bfrtip",
          targets: 'no-sort', 'bSort': false, 'order': [],
          ajax: {
              'url': '/Equipment/GetEquipmentData',
              'type': 'POST',
              'dataType': 'JSON'
          },
          buttons: [
            {
              extend: "copy",
              className: "btn-sm",
              exportOptions: {
                  columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
              }
            },
            {
              extend: "csv",
              className: "btn-sm",
              exportOptions: {
                  columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
              }
            },
            {
              extend: "excel",
              className: "btn-sm",
              exportOptions: {
                  columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
              }
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm",
              exportOptions: {
                  columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
              }
            },
            {
              extend: "print",
              className: "btn-sm",
              title: "List Of Items Report",
              exportOptions: {
                  columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
              }
            },
          ],
          columns:[
              {'data': 'name'},
              {'data': 'brand'},
              {'data': 'model'},
              {'data': 'price'},
              {'data': 'quantity'},
              {'data': 'total'},
              {
                'width':'8%',
                'data': null,
                'render': function(data, type, row){
                    var badgeColor = null;
                    switch(row.itemStatus){
                        case 'Available': {badgeColor='green';}break;
                        case 'Reserved': {badgeColor='blue';}break;
                        case 'Borrowed': {badgeColor='orange';}break;
                        default: {badgeColor='red';}break;
                    }
                    return '<span class="badge bg-'+ badgeColor +'">'+ row.itemStatus +'</span>';
                }
            },
              {'data': 'addedBy'},
              {'data': 'dateAdded'},
              {'data': 'dateModified'},
              {
                'data': null,
                width: '10%',
                render : function(data, type, row){
                    var edit = '<a href="/Equipment/EditItemModel/'+ row.key +'" id="btnEdit" class="btn btn-xs btn-info  btn-block"><i class="fa fa-edit"></i> Edit</a>';
                    return edit;
                }
            },
          ],
          responsive: true
        });
      }
    };

    $(".dataTables_filter input").unbind().bind('keyup change', function (e) {
      if (e.keyCode == 13 || this.value == "") {
          $table.search(this.value)
                .draw();
      }
  });

  $('#tblEquipment tbody').on('click', '#btnRemove', function(){
    var row = $table.row($(this).parents('tr')).data();
    swal({
        title: 'Delete '+ row.name +'->'+ row.brand,
        text: "Are you sure to remove this?",
        type: 'question',
        allowOutsideClick: false,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then((result) => {
        if (result.value) {
            $.ajax({
                url: '/Equipment/Remove',
                type: 'get',
                dataType: 'json',
                data: {id: row.id, key: row.key},
                success: function(d){
                    if(d.status){
                        $table.ajax.reload();
                        return false;
                    }
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: d.message
                    })
                }
            })
            console.log('Removed');
            return false;
        }
      })
});

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();

    TableManageButtons.init();
  });
