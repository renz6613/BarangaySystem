$table = null;

$(document).ready(function () {
  var handleDataTableButtons = function () {
    if ($("#tblPersons").length) {
      $table = $("#tblPersons").DataTable({
        lengthMenu: [5, 10, 15],
        processing: true,
        serverSide: true,
        autoWidth: false,
        dom: "Bfrtip",
        targets: 'no-sort',
        'bSort': false,
        'order': [],
        ajax: {
          'url': '/Person/GetPersonsData',
          'type': 'POST',
          'dataType': 'JSON'
        },
        buttons: [{
            extend: "copy",
            className: "btn-sm",
            exportOptions: {
              columns: [ 0, 1,2,3,4,5,6,7]
            }
          },
          {
            extend: "csv",
            className: "btn-sm",
            exportOptions: {
              columns: [ 0, 1,2,3,4,5,6,7]
            }
          },
          {
            extend: "excel",
            className: "btn-sm",
            exportOptions: {
              columns: [ 0, 1,2,3,4,5,6,7]
            }
          },
          {
            extend: "pdfHtml5",
            className: "btn-sm",
            exportOptions: {
              columns: [ 0, 1,2,3,4,5,6,7]
            }
          },
          {
            extend: "print",
            className: "btn-sm",
            title: "Residence Record Report",
            exportOptions: {
              columns: [ 0, 1,2,3,4,5,6,7]
            }
          },
        ],
        columns: [
          {
            'data': 'fname'
          },
          {
            'data': 'mname'
          },
          {
            'data': 'lname'
          },
          {
            'data': 'age'
          },
          {
            'data': 'gender'
          },
          {
            'data': 'email'
          },
          {
            'data': 'address'
          },
          {
            'data': 'contactNumber'
          },
          {

            'data': null,
            width: '10%',
            render: function (data, type, row) {
              var edit = '<a href="/Person/EditPerson/'+ row.key +'" id="btnEdit" class="btn btn-xs btn-info  btn-block"><i class="fa fa-edit"></i>Edit</a>';
                  // view = '<a href="/Person/ViewBorrowed?key='+ row.key +'" id="btnView" class="btn btn-xs btn-default  btn-block"><i class="fa fa-eye"></i>View</a>';
              //     var menu = '<div class="btn-group">'+
              //     '<button type="button" class="btn btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">'+
              //         '<i class="fa fa-cogs fa-lg"></i>'+
              //         '<div class="ripple-container"></div>'+
              //     '</button>'+
              //     '<ul class="dropdown-menu dropdown-menu-right">'+
              //         '<li>'.concat(edit,'</li>')+
              //         '<li>'.concat(borrow,'</li>')+
              //         '<li>'.concat(release,'</li>')+
              //         '<li>'.concat(returns,'</li>')+
              //         '<li>'.concat(remove,'</li>')+
              //     '</ul>'+
              // '</div>';
                return edit;
            }
          }
        ],
        responsive: true
      });
    }
  };
//   $('#tblPersons').on('change', '.ctrl-status', function(){
//     var data = $('#tblPersons').DataTable().row($(this).closest('tr')).data();
//     // alert(data[0] + ' Selected:' + $(this).val());
// });

  $(".dataTables_filter input").unbind().bind('keyup change', function (e) {
    if (e.keyCode == 13 || this.value == "") {
      $table.search(this.value)
        .draw();
    }
  });

  $('#tblPersons tbody').on('click', '#btnRemove', function () {
    var row = $table.row($(this).parents('tr')).data();
    swal({
      title: 'Delete ' + row.fname + ' ' + row.mname + ' ' + row.lname,
      text: "Are you sure to remove this?",
      type: 'question',
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '/Person/Remove',
          type: 'get',
          dataType: 'json',
          data: {
            id: row.id,
            key: row.key
          },
          success: function (d) {
            if (d.status) {
              alert('Record Successfully Removed');
              $table.ajax.reload();
              return false;
            }
            swal({
              type: 'error',
              title: 'Oops...',
              text: d.message
            })
          }
        })
        console.log('Removed');
        return false;
      }
    })
  });

  // $('#tblPersons tbody').on('click','#btnReturn',function(){
  //   var row = $table.row($(this).parents('tr')).data();
  //   $.ajax({
  //       'url': '/Person/UpdateStatus',
  //       'type': 'POST',
  //       'dataType': 'JSON',
  //       'data': { id: row.id, status: row.status, name: row.name },
  //       beforeSend: function(){
  //           //
  //       },
  //       success: function(res){
  //           if(res.status){
  //               $table.ajax.reload();
  //           }
  //       }
  //   })
  //   console.log(row);
  // });

  TableManageButtons = function () {
    "use strict";
    return {
      init: function () {
        handleDataTableButtons();
      }
    };
  }();

  TableManageButtons.init();
});