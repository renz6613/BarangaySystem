$(function(){
    $table = $("#tblBusiness").DataTable({
        lengthMenu: [5, 10, 15],
        processing: true,
        serverSide: true,
        autoWidth: false,
        dom: "Bfrtip",
        targets: 'no-sort',
        'bSort': false,
        'order': [],
        ajax: {
          'url': '/Business/GetBusiness',
          'type': 'POST',
          'dataType': 'JSON'
        },
        buttons: [
          {
            extend: "print",
            className: "btn-sm",
            title: "Business Permit Reports",
            exportOptions: {
              columns: [ 0, 1,2,3,4,5,6]
            }
          },
        ],
        columns: [{
            'data': 'date'
          },
          {
            'data': 'name'
          },
          {
            'data': 'business'
          },
          {
            'data': 'address'
          },
          {
            'data': 'totalTax'
          },
          {
            'data': 'typeOfRequest'
          },
          {
            'data': 'releasedBy'
          },
          {

            'data': null,
            width: '10%',
            render: function (data, type, row) {
                var Edit = '<a href="/Business/EditBusiness/'+ row.key +'" class="btn btn-xs btn-primary btn-block" id="btnEdit"><i class="fa fa-gear"></i> Edit</a>',
                    Print = '<a href="/Print/printBusiness/'+ row.key +'" class="btn btn-xs btn-info  btn-block"><i class="fa fa-print"></i> Print</a>',
                    Or = '<a href="/Print/OR/'+ row.key +'" class="btn btn-xs btn-success  btn-block"><i class="fa fa-print"></i> OR</a>';
                    return Edit.concat(Print,Or);
            }
          }
        ],
        responsive: true
      });
});