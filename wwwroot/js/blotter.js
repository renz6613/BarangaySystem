$table = null;
$(document).ready(function() {
    var handleDataTableButtons = function() {
      if ($("#tblBlotter").length) {
        $table = $("#tblBlotter").DataTable({
            lengthMenu: [5, 10, 15],
            processing: true,
            serverSide: true,
            autoWidth: false,
          dom: "Bfrtip",
          targets: 'no-sort', 'bSort': false, 'order': [],
          ajax: {
              'url': '/Blotter/GetBlotterData',
              'type': 'POST',
              'dataType': 'JSON'
          },
          buttons: [
            {
              extend: "copy",
              className: "btn-sm",
              exportOptions: {
                  columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
              }
            },
            {
              extend: "csv",
              className: "btn-sm",
              exportOptions: {
                  columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
              }
            },
            {
              extend: "excel",
              className: "btn-sm",
              exportOptions: {
                  columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
              }
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm",
              exportOptions: {
                  columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
              }
            },
            {
              extend: "print",
              className: "btn-sm",
              title: "Blotter Report",
              exportOptions: {
                  columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
              }
            },
            {
              extend: "colvis",
              className: "btn-sm"
            },
          ],
          columns:[
              {'data': 'name'},
              {'data': 'age'},
              {'data': 'address'},
              {'data': 'nameOfRespondent'},
              {'data': 'respondentAddress'},
              {'data': 'respondentAge'},
              {'data': 'purpose'},
              {'data': 'bpsoonDuty'},
              {'data': 'facts'},
              {'data': 'dateRecorded'},
              {
                'data': null,
                width: '10%',
                render : function(data, type, row){
                    var edit = '<a href="/Blotter/EditBlotter/'+ row.key +'" id="btnEdit" class="btn btn-xs btn-info  btn-block"><i class="fa fa-edit"></i> Edit</a>',
                        desc = '<a href="/Blotter/ViewDesc/'+ row.key +'" id="btndesc" class="btn btn-xs btn-default  btn-block"><i class="fa fa-eye"></i> Facts</a>';
                    return edit.concat(desc);
                }
            }
          ],
          responsive: true
        });
      }
    };

    $(".dataTables_filter input").unbind().bind('keyup change', function (e) {
      if (e.keyCode == 13 || this.value == "") {
          $table.search(this.value)
                .draw();
      }
  });

  $('#tblBlotter tbody').on('click', '#btnRemove', function(){
    var row = $table.row($(this).parents('tr')).data();
    swal({
        title: 'Delete '+ row.name +'->'+ row.nameOfRespondent,
        text: "Are you sure to remove this?",
        type: 'question',
        allowOutsideClick: false,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then((result) => {
        if (result.value) {
            $.ajax({
                url: '/Blotter/Remove',
                type: 'get',
                dataType: 'json',
                data: {id: row.id, key: row.key},
                success: function(d){
                    if(d.status){
                        $table.ajax.reload();
                        return false;
                    }
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: d.message
                    })
                }
            })
            console.log('Removed');
            return false;
        }
      })
});

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();

    TableManageButtons.init();
  });
