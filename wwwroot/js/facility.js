$table = null;
$(document).ready(function() {
    var handleDataTableButtons = function() {
      if ($("#tblFacility").length) {
        $table = $("#tblFacility").DataTable({
            lengthMenu: [5, 10, 15],
            processing: true,
            serverSide: true,
            autoWidth: false,
          dom: "Bfrtip",
          targets: 'no-sort', 'bSort': false, 'order': [],
          ajax: {
              'url': '/Facility/GetFacilitiesData',
              'type': 'POST',
              'dataType': 'JSON'
          },
          buttons: [
            {
              extend: "copy",
              className: "btn-sm",
              exportOptions: {
                columns: [ 0, 1 ]
              }
            },
            {
              extend: "csv",
              className: "btn-sm",
              exportOptions: {
                columns: [ 0, 1 ]
              }
            },
            {
              extend: "excel",
              className: "btn-sm",
              exportOptions: {
                columns: [ 0, 1 ]
              }
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm",
              exportOptions: {
                columns: [ 0, 1 ]
              }
            },
            {
              extend: "print",
              className: "btn-sm",
              title: "Facility Report",
              exportOptions: {
                columns: [ 0, 1 ]
              }
            },
          ],
          columns:[
              {'data': 'name'},
              {'data': 'description'},
              {
                'data': null,
                width: '10%',
                render : function(data, type, row){
                    var edit = '<a href="/Facility/EditFacility/'+ row.key +'" id="btnEdit" class="btn btn-xs btn-info  btn-block"><i class="fa fa-edit"></i> Edit</a>';
                    return edit;
                }
            }
          ],
          responsive: true
        });
      }
    };

    $(".dataTables_filter input").unbind().bind('keyup change', function (e) {
      if (e.keyCode == 13 || this.value == "") {
          $table.search(this.value)
                .draw();
      }
  });

  $('#tblFacility tbody').on('click', '#btnRemove', function(){
    var row = $table.row($(this).parents('tr')).data();
    swal({
        title: 'Delete '+ row.name,
        text: "Are you sure to remove this?",
        type: 'question',
        allowOutsideClick: false,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then((result) => {
        if (result.value) {
            $.ajax({
                url: '/Facility/Remove',
                type: 'get',
                dataType: 'json',
                data: {id: row.id, key: row.key},
                success: function(d){
                    if(d.status){
                        $table.ajax.reload();
                        return false;
                    }
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: d.message
                    })
                }
            })
            console.log('Removed');
            return false;
        }
      })
});

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();

    TableManageButtons.init();
  });
