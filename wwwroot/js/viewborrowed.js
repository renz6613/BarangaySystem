$table = null;

$(document).ready(function () {

  var handleDataTableButtons = function () {
    if ($("#tblBorroweds").length) {
      $table = $("#tblBorroweds").DataTable({
        lengthMenu: [5, 10, 15],
        processing: true,
        serverSide: true,
        autoWidth: false,
        dom: "Bfrtip",
        targets: 'no-sort',
        'bSort': false,
        'order': [],
        ajax: {
          'url': '/Person/GetBorrowData',
          'type': 'POST',
          'dataType': 'JSON'
        },
        buttons: [{
            extend: "copy",
            className: "btn-sm",
            exportOptions: {
              columns: [ 0, 1,2,3,4,5,6,]
            }
          },
          {
            extend: "csv",
            className: "btn-sm",
            exportOptions: {
              columns: [ 0, 1,2,3,4,5,6,]
            }
          },
          {
            extend: "excel",
            className: "btn-sm",
            exportOptions: {
              columns: [ 0, 1,2,3,4,5,6,]
            }
          },
          {
            extend: "pdfHtml5",
            className: "btn-sm",
            exportOptions: {
              columns: [ 0, 1,2,3,4,5,6,]
            }
          },
          {
            extend: "print",
            className: "btn-sm",
            exportOptions: {
              columns: [ 0, 1,2,3,4,5,6,],
              title: "Borrower's Report"
            }
            // title: "Leano",
            // customize: function ( win ) {
            //     $(win.document.body)
            //         .css( 'font-size', '10pt' )
            //         .prepend(
            //             '<img src="home/raymond/6Sambag1.png" style="width:30px; height:30px;" alt="BRGY">'
            //         );

            //     $(win.document.body).find( 'table' )
            //         .addClass( 'compact' )
            //         .css( 'font-size', 'inherit' );
            // }
          },
        ],
        columns: [
          {
            'data': 'personBorrower'
          },
          {
            'data': 'itemName'
          },
          {
            'data': 'itemQuantity'
          },
          {
            'data': 'borrowStatus'
          },
          {
            'data': 'dateReserved'
          },
          {
            'data': 'dateReleased'
          },
          {
            'data': 'dateToReturn'
          },
          {

            'data': null,
            width: '10%',
            render: function (data, type, row) {
              var release = '<a href="#"  id="btnRelease">Release Item</a>',
                  returns = '<a href="#" id="btnReturn"> Return Item</a>',
                  slip = '<a href="/BorrowItem/BorrowersSlip?key='+ row.key +'"> Borrow Slip </a>';
                  var menu = '<div class="btn-group">'+
                  '<button type="button" class="btn btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">'+
                      '<i class="fa fa-cogs fa-lg"></i>'+
                      '<div class="ripple-container"></div>'+
                  '</button>'+
                  '<ul class="dropdown-menu dropdown-menu-right">'+
                      '<li>'.concat(release,'</li>')+
                      '<li>'.concat(returns,'</li>')+
                      '<li>'.concat(slip,'</li>')+
                  '</ul>'+
              '</div>';
                return menu;
            }
          }
        ],
        responsive: true
      });
    }
  };
//   $('#tblPersons').on('change', '.ctrl-status', function(){
//     var data = $('#tblPersons').DataTable().row($(this).closest('tr')).data();
//     // alert(data[0] + ' Selected:' + $(this).val());
// });

  $(".dataTables_filter input").unbind().bind('keyup change', function (e) {
    if (e.keyCode == 13 || this.value == "") {
      $table.search(this.value)
        .draw();
    }
  });

  $('#tblBorroweds tbody').on('click','#btnRelease',function(){
    var row = $table.row($(this).parents('tr')).data();
    $.ajax({
        'url': '/BorrowItem/Release',
        'type': 'POST',
        'dataType': 'JSON',
        'data': { id: row.id, key: row.key, status: row.status},
        beforeSend: function(){
            //
        },
        success: function(res){
            if(res.status){
              alert('Successfully Released');
                $table.ajax.reload();
                return false;
            }
        alert('No Item to Release');
        }
    })
    console.log(row);
  });

  $('#tblBorroweds tbody').on('click','#btnReturn',function(){
    var row = $table.row($(this).parents('tr')).data();
    $.ajax({
        'url': '/BorrowItem/Return',
        'type': 'POST',
        'dataType': 'JSON',
        'data': { id: row.id, key: row.key, status: row.status},
        beforeSend: function(){
            //
        },
        success: function(res){
            if(res.status){
              alert(' Item has been successfully returned');
                $table.ajax.reload();
                return false;
            }
        alert('No Item to Return.!');
        }
    })
    console.log(row);
  });

  $('#tblBorroweds tbody').on('click', '#btnRemove', function () {
    var row = $table.row($(this).parents('tr')).data();
    swal({
      title: 'Delete ' + row.fname + ' ' + row.mname + ' ' + row.lname,
      text: "Are you sure to remove this?",
      type: 'question',
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '/Person/Remove',
          type: 'get',
          dataType: 'json',
          data: {
            id: row.id,
            key: row.key
          },
          success: function (d) {
            if (d.status) {
              alert('Record Successfully Removed');
              $table.ajax.reload();
              return false;
            }
            swal({
              type: 'error',
              title: 'Oops...',
              text: d.message
            })
          }
        })
        console.log('Removed');
        return false;
      }
    })
  });

  // $('#tblPersons tbody').on('click','#btnReturn',function(){
  //   var row = $table.row($(this).parents('tr')).data();
  //   $.ajax({
  //       'url': '/Person/UpdateStatus',
  //       'type': 'POST',
  //       'dataType': 'JSON',
  //       'data': { id: row.id, status: row.status, name: row.name },
  //       beforeSend: function(){
  //           //
  //       },
  //       success: function(res){
  //           if(res.status){
  //               $table.ajax.reload();
  //           }
  //       }
  //   })
  //   console.log(row);
  // });

  TableManageButtons = function () {
    "use strict";
    return {
      init: function () {
        handleDataTableButtons();
      }
    };
  }();

  TableManageButtons.init();
});