
$(document).ready(function () {
    window.print();
    var name = $('#pName').val();
    var birth = $('#pDateOfBirth').val();
    var address = $('#pAddress').val();
    var purpose = $('#pPurpose').val();
    var dateReleased = $('#pDateReleased').val();
    var dateIssued = $('#pIssuedDate').val();


    $('#Name').text(name);
    $('#DateOfBirth').text(birth);
    $('#Address').text(address);
    $('#Purpose').text(purpose);
    $('#DateReleased').text(dateReleased);
    $('#IssuedDate').text(dateIssued);


});
