$table = null;
$(document).ready(function() {
    var handleDataTableButtons = function() {
      if ($("#tblInventory").length) {
        $table = $("#tblInventory").DataTable({
            lengthMenu: [20],
            processing: true,
            serverSide: true,
            autoWidth: false,
          dom: "Brtip",
          targets: 'no-sort', 'bSort': false, 'order': [],
          ajax: {
              'url': '/Equipment/GetInventory',
              'type': 'POST',
              'dataType': 'JSON'
          },
          buttons: [
            {
              extend: "copy",
              className: "btn-sm"
            },
            {
              extend: "csv",
              className: "btn-sm"
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm",
              title: "Inventory Report"
            },
          ],
          columns:[
              {'data': 'name'},
              {'data': 'price'},
              {'data': 'quantity'},
              {'data': 'total'},
              {'data': 'borrowedItem'}
          ],
          responsive: true
        });
      }
    };

    $(".dataTables_filter input").unbind().bind('keyup change', function (e) {
      if (e.keyCode == 13 || this.value == "") {
          $table.search(this.value)
                .draw();
      }
  });

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();

    TableManageButtons.init();
  });
