jQuery(function($){

    // $('#DateTime').daterangepicker({
    //     singleDatePicker: true,
    //     allowInputToggle: true,
    //     viewMode: 'days',
    //     format: 'MM/DD/YYYY',
    //     defaultDate: new Date()
    // }).on('dp.change', function(e) {
    //     var date = e.date.format('L');
    //     jQuery('#DateTime').val(date); 
    // });
    
    // <!-- bootstrap-daterangepicker -->

        $('#DateTime').daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          minYear: 1901,
          maxYear: parseInt(moment().format('YYYY'),10),
          calender_style: "picker_4",
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

});