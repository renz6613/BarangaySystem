$(document).ready(function () {
    $table = $("#tblPayment").DataTable({
        lengthMenu: [5, 10, 15],
        processing: true,
        serverSide: true,
        autoWidth: false,
        dom: "Bfrtip",
        targets: 'no-sort',
        'bSort': false,
        'order': [],
        ajax: {
          'url': '/Payment/GetPayments',
          'type': 'POST',
          'dataType': 'JSON'
        },
        buttons: [
          {
            extend: "print",
            className: "btn-sm"
          },
        ],
        columns: [{
            'data': 'name'
          },
          {
            'data': 'permitFee'
          },
          {
            'data': 'php'
          },
          {
            'data': 'permitFee1'
          },
          {
            'data': 'php1'
          },
          {
            'data': 'permitFee2'
          },
          {
            'data': 'php2'
          },
          {
            'data': 'permitFee3'
          },
          {
            'data': 'php3'
          },
          {
            'data': 'totalTax'
          }
        ],
        responsive: true
      });
});