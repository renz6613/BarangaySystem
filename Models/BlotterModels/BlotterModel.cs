using System.ComponentModel.DataAnnotations;

namespace BarangaySystem.Models.BlotterModels
{
    public class BlotterModel
    { 
        public int Id { get; set; }
        public string Key { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Name { get; set; }
        [Required (ErrorMessage="This field is required")]
        public int Age { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Address { get; set; }
        public string NameOfRespondent { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Facts { get; set; }
        public string RespondentAddress { get; set; }
        public int RespondentAge { get; set; }
        public string Purpose { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string BpsoonDuty { get; set; }
        public string DateRecorded {get; set;}
    }

    public class BlotterViewModel
    { 
        public string Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Age { get; set; }
        public string Address { get; set; }
        public string NameOfRespondent { get; set; }
        public string Facts { get; set; }
        public string RespondentAddress { get; set; }
        public string RespondentAge { get; set; }
        public string Purpose { get; set; }
        public string BpsoonDuty { get; set; }
        public string DateRecorded {get; set;}
    }
}