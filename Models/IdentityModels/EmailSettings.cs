namespace BarangaySystem.Models.IdentityModels
{
    public class EmailSettings
    {
        public string PrimaryDomain { get; set; }
        public int PrimaryPort { get; set; }
        public string UsernameEmail { get; set; }
        public string UsernamePassword { get; set; }
        public string FromEmail { get; set; }
        public string HtmlMessagePrefix { get; set; }
        public string HtmlMessageSuffix { get; set; }
        public string HtmlFooterMessage { get; set; }

    }

}