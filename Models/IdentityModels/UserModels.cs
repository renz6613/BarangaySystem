using System.ComponentModel.DataAnnotations;

namespace BarangaySystem.Models.IdentityModels
{
    public class LoginModels
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }

     public class UserProfileModel
    {
        [Required(ErrorMessage="This field is required")]
        public string Name { get; set; }

        [Required(ErrorMessage="This field is required")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage="This field is required")]
        public string ContactNumber { get; set; }
    }

    public class UserProfileViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string RoleName { get; set; }
        public bool Status { get; set; }
    }


    public class ChangePassModel
    {
        [Required]
        public string CurrentPass { get; set; }

        [Required]
        public string NewPass { get; set; }

        [Required, Compare(nameof(NewPass))]
        public string ConfirmPass { get; set; }
    }

    public class ForgotPasswordModel
    {
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage="This email is required")]
        public string EmailAddress { get; set; }
    }

    public class ResetPasswordViewModel
    {
        public string Code { get; set; }

        [Required(ErrorMessage="This field is required")]

        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage="This field is required")]

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage="This field is required")]

        [DataType(DataType.Password)]
        [Compare(nameof(Password))]
        public string ConfirmPassword { get; set; }
    }
}