using System;
using Microsoft.AspNetCore.Identity;

namespace BarangaySystem.Models.IdentityModels
{
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// Fullname of a user
        /// </summary>
        /// <value>Fullname</value>
        public string Fullname { get; set; }

        /// <summary>
        /// An Old password used
        /// </summary>
        /// <value>Old Password</value>
        public string OldPassword { get; set; }

        /// <summary>
        /// AN image of the user
        /// </summary>
        /// <value>Image filename stored in the folder</value>
        public string ImageFilename { get; set; }


        /// <summary>
        /// Date and time created of the user
        /// </summary>
        /// <value>Date and Time</value>
        public DateTime DateCreated { get; set; }
    }

    public class ApplicationRole : IdentityRole
    {
        /// <summary>
        /// Description of the role created
        /// </summary>
        /// <value>Description</value>
        public string Description { get; set; }
    }
}