namespace BarangaySystem.Models.PaymentModels
{
    public class PaymentModel
    {
        public string Name { get; set; }
        public string PermitFee { get; set; }
        public string Php { get; set; }
        public string PermitFee1 { get; set; }
        public string Php1 { get; set; }
        public string PermitFee2 { get; set; }
        public string Php2 { get; set; }
        public string PermitFee3 { get; set; }
        public string Php3 { get; set; }
        public decimal TotalTax { get; set; }
    }
}