using System.ComponentModel.DataAnnotations;

namespace BarangaySystem.Models.PersonModels
{
    public class PersonModel
    {
        public int Id { get; set; }
        public string Key { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Fname { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Mname { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Lname { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Gender { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Address { get; set; }
        [Required (ErrorMessage="This field is required"), MaxLength(11)]
        public string ContactNumber { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        [Required (ErrorMessage="This field is required")]
        public int Age { get; set; }
        [Required (ErrorMessage="This field is required")]
        public int ModelId { get; set; }
        [Required (ErrorMessage="This field is required")]
        public int ReturnedItem { get; set; }
        public string Status { get; set; }
    }

    public class PersonViewModel
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string Fname { get; set; }
        public string Mname { get; set; }
        public string Lname { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public string Age { get; set; }
        public string ModelItem { get; set; }
        public string ReturnedItem { get; set; }
        public string Quantity { get; set; }
        public string Status { get; set; }
    }

    public class PersonBorrowSlip
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string Fname { get; set; }
        public string Mname { get; set; }
        public string Lname { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public string Age { get; set; }
        public string ModelItem { get; set; }
        public string ReturnedItem { get; set; }
        public string Quantity { get; set; }
        public string DateReturn { get; set; }
        public string DateReserved { get; set; }
        public string DateReleased { get; set; }
        public string IssuedBy { get; set; }
        public string BorrowedTime { get; set; }
    }
}