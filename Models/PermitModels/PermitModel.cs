using System;
using System.ComponentModel.DataAnnotations;

namespace BarangaySystem.Models.PermitModels
{
    public class BarangayModel
    {
        public int Id { get; set; }
        public string Key { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Name { get; set; }
        public string ReleasedBy { get; set; }
        [Required (ErrorMessage="This field is required")]
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Purpose { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string IssuedDate { get; set; }
    }
    public class BarangayViewModel
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string ReleasedBy { get; set; }
        public string DateRequested { get; set; }
        public string DateOfBirth { get; set; }
        public string Address { get; set; }
        public string Purpose { get; set; }
        public string IssuedDate { get; set; }
        public string DateReleased { get; set; }
        
    }

    public class BusinessViewModel
    {
        public string Key { get; set; }
         public string Date { get; set; }
         public string Name { get; set; }
         public string Business { get; set; }
         public string Address { get; set; }
         public string TotalTax { get; set; }
         public string ReleasedBy { get; set; }
         public string TypeOfRequest { get; set; }
    }

    public class BusinessModel
    {
        public string ReleasedBy { get; set; }
        public string Id { get; set; }

        [Required (ErrorMessage="This field is required")]
        public string Name { get; set; }

        [Required (ErrorMessage="This field is required")]
        public string BusinessName { get; set; }

        [Required (ErrorMessage="This field is required")]
        public DateTime DateRequested { get; set; }

        [Required (ErrorMessage="This field is required")]
        public string BusinessAddress { get; set; }

        [Required (ErrorMessage="This field is required")]
        public int Bbpno { get; set; }

        [Required (ErrorMessage="This field is required")]
        public string TypeOfRequest { get; set; }
        
        [Required (ErrorMessage="This field is required")]
        public string AssessedBy { get; set; }

        
        [Required (ErrorMessage="This field is required")]
        public string PermitFee { get; set; }


        [Required (ErrorMessage="This field is required")]
        public decimal Php { get; set; }

        public string PermitFee1 { get; set; }
        public decimal? Php1 { get; set; }

        public string PermitFee2 { get; set; }
        public decimal? Php2 { get; set; }
        
        public string PermitFee3 { get; set; }
        public decimal? Php3 { get; set; }
        public decimal TotalTax { get; set; }
    }
    public class BusinessPrint
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string BusinessName { get; set; }
        public string ReleasedBy { get; set; }
        public string DateReleased { get; set; }
        public string DateRequested { get; set; }
        public string BusinessAddress { get; set; }
        public string BBPno { get; set; }
        public string AssessedBy { get; set; }
        public string PermitFee { get; set; }
        public string PHP { get; set; }
        public string PermitFee1 { get; set; }
        public string PHP1 { get; set; }
        public string PermitFee2 { get; set; }
        public string PHP2 { get; set; }
        public string PermitFee3 { get; set; }
        public string PHP3 { get; set; }
        public string TotalTax { get; set; }
        public string TypeOfRequest { get; set; }
        
    }
}