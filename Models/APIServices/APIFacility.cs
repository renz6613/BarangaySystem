using System;
using System.Collections.Generic;
using System.Linq;
using BarangaySystem.Data;
using BarangaySystem.Models.FacilityModels;
using BarangaySystem.Utilities;
using Microsoft.EntityFrameworkCore;

namespace BarangaySystem.Models.APIServices
{
    public class APIFacility
    {
        public bool Add(FacilityModel facilityModel, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                if(db.Facility.Where(x => x.Name == facilityModel.Name).Any ()) {
                    message = "Oppss..!! This Facility is already exist..!!";
                    return false;
                }


                var em  = new Facility {
                    Name = facilityModel.Name,
                    Description = facilityModel.Description
                };
                db.Entry (em).State = EntityState.Added;

                    var act = new ActivityLogs();
                    act.Description = $"The facility {facilityModel.Name} was added by {addedBy}";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;

                bool flag = db.SaveChanges () > 0;

                message = flag ? "Successfully Added!" : "Insert failed";
                return flag;
            }
        }

        public FacilityModel GetFacility (int id) {
            using (var db = new DbBarangayContext ()) {
                var em = db.Facility.FirstOrDefault ( x => x.Id == id);
                var facility = new FacilityModel {
                    Id = em.Id,
                    Key = em.Id.ToString().EncryptString(),
                    Name = em.Name,
                    Description = em.Description
                };
                return facility;
            }
        }

         public bool Update(FacilityModel facilityModel, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                if(db.Facility.Where(x => x.Name == facilityModel.Name).Any ()) {
                    message = "Oppss..!! This Facility is already exist..!!";
                    return false;
                }

                var em  = db.Facility.FirstOrDefault(x => x.Id == facilityModel.Id);
                em.Name = facilityModel.Name;
                em.Description = facilityModel.Description;

                db.Entry (em).State = EntityState.Modified;

                    var act = new ActivityLogs();
                    act.Description = $"The facility {facilityModel.Name} was Updated by {addedBy}";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;

                bool flag = db.SaveChanges () > 0;
                message = flag? "Successfully Updated!" : "Insert failed";
                return flag;
            }
        }

        public bool Delete (int id, string addedBy, out string message) {
            using(var db = new DbBarangayContext ()) {
                message = string.Empty;
                var em = db.Facility.FirstOrDefault (x => x.Id == id);
                db.Entry (em).State = EntityState.Deleted;

                    var act = new ActivityLogs();
                    act.Description = $"The facility {em.Name} was removed by {addedBy}";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;

                bool flag = db.SaveChanges () > 0;
                message = flag? "Successfully Deleted" : "Delete Failed";
                return flag;
            }
        }

        public List<FacilityViewModel> GetFacilityData(int start , int pageSize, out int count) {
            using (var db= new DbBarangayContext ()){

                    var query = db.Facility;
                    count = query.Count();
                    var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);
                    var facility = new List<FacilityViewModel> ();
                    foreach (var em in rows) {
                        string id = em.Id.ToString();
                        facility.Add (new FacilityViewModel {
                            Id = id,
                            Key = id.EncryptString(),
                            Name = em.Name,
                            Description = em.Description
                        });
                    }
                    return facility;
                }
        }

        public List<FacilityViewModel> SearchFacilityData(string key, int start , int pageSize, out int count) {
            using (var db= new DbBarangayContext ()){
                    var query = db.Facility.Where(x=> x.Name.StartsWith(key));
                    count = query.Count();
                    var rows = query.OrderByDescending(x => x.Id).Take(pageSize).Skip(start);
                    var facility = new List<FacilityViewModel> ();
                    foreach (var em in rows) {
                        string id = em.Id.ToString();
                        facility.Add (new FacilityViewModel {
                            Id = id,
                            Key = id.EncryptString(),
                            Name = em.Name,
                            Description = em.Description
                        });
                    }
                    return facility;
                }
        }
    }
}