using System;
using System.Collections.Generic;
using System.Linq;
using BarangaySystem.Data;
using BarangaySystem.Models.ItemModels;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BarangaySystem.Models.APIServices
{
    public class APIItems
    {
        public bool Add(ItemModel model, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                if(db.Items.Where(x => x.Name == model.Name).Any ()) {
                    message = "Oppss..!! This Item is already exist..!!";
                    return false;
                }

                var em  = new Items {
                    Name = model.Name,
                    Brand = model.Brand,
                    AddedBy = addedBy,
                    DateAdded = DateTime.Now
                };
                db.Entry (em).State = EntityState.Added;
                bool flag = db.SaveChanges () > 0;

                message = flag ? "Successfully Added!" : "Insert failed";
                return flag;
            }
        }

         /// <summary>
        /// Updates the items data
        /// </summary>
        /// <param name="itemModel">the items data</param>
        /// <param name="addedBy">who updates the item</param>
        /// <param name="message"></param>
        /// <returns>the updated item</returns>
        public bool Update(ItemModel itemModel, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                if(db.Items.Where (x => x.Name == itemModel.Name && x.Brand == itemModel.Brand).Any ()) {
                    message = "Oppss..!! This Item is already exist..!!";
                    return false;
                }

                var em  = db.Items.FirstOrDefault(x => x.Id == itemModel.Id);
                    em.Name = itemModel.Name;
                    em.Brand = itemModel.Brand;
                    em.AddedBy = addedBy;
                    em.DateModified = DateTime.Now;

                db.Entry (em).State = EntityState.Modified;
                bool flag = db.SaveChanges () > 0;
                message = flag? "Successfully Updated!" : "Insert failed";
                return flag;
            }
        }

        /// <summary>
        /// Get the Items Data for Edit
        /// </summary>
        /// <param name="id">items Id</param>
        /// <returns>the Items Data</returns>
        public ItemModel GetItem (int id) {
            using (var db = new DbBarangayContext ()) {
                var em = db.Items.FirstOrDefault ( x => x.Id == id);
                var items = new ItemModel {
                    Id = em.Id,
                    Key = em.Id.ToString().EncryptString(),
                    Name = em.Name,
                    Brand = em.Brand,
                    AddedBy = em.AddedBy
                };
                return items;
            }
        }
        
        [HttpPost]
        public List<ItemViewModel> GetItemData(int start , int pageSize, out int count) {
            using (var db= new DbBarangayContext ()){

                    var query = db.Items;
                    count = query.Count();
                    var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);
                    var items = new List<ItemViewModel> ();
                    foreach (var em in rows) {
                        string id = em.Id.ToString();
                        items.Add (new ItemViewModel {
                            Id = id,
                            Key = id.EncryptString(),
                            Name = em.Name,
                            Brand = em.Brand,
                            Models = db.Models.Where(x => x.ItemId == em.Id).Count().ToString(),
                            AddedBy = em.AddedBy,
                            DateAdded = em.DateAdded.ToString(),
                            DateModified = em.DateModified.ToString()
                        });
                    }
                    return items;
                }
        }

        /// <summary>
        /// Search the items
        /// </summary>
        /// <param name="key">Any keywords to be search</param>
        /// <param name="start">Row of an offset to get</param>
        /// <param name="pageSize">Number of rows to fetch</param>
        /// <param name="count">Total of records</param>
        /// <returns>Returns the data fetched</returns>
        public List<ItemViewModel> SearchItemsData(string key, int start , int pageSize, out int count) {
            using (var db= new DbBarangayContext ()){
                    var query = db.Items.Where(x=> x.Name.StartsWith(key) || x.Brand.StartsWith(key));
                    count = query.Count();
                    var rows = query.OrderByDescending(x => x.Id).Take(pageSize).Skip(start);
                    var items = new List<ItemViewModel> ();
                    foreach (var em in rows) {
                        string id = em.Id.ToString();
                        items.Add (new ItemViewModel {
                            Id = id,
                            Key = id.EncryptString(),
                            Name = em.Name,
                            Brand = em.Brand,
                            Models = db.Models.Where(x => x.ItemId == em.Id).Count().ToString(),
                            AddedBy = em.AddedBy,
                            DateAdded = em.DateAdded.ToString(),
                            DateModified = em.DateModified.ToString()
                        });
                    }
                    return items;
                }
        }

         public bool Delete (int id, out string message) {
            using(var db = new DbBarangayContext ()) {
                message = string.Empty;
                var em = db.Items.FirstOrDefault (x => x.Id == id);
                db.Entry (em).State = EntityState.Deleted;
                bool flag = db.SaveChanges () > 0;
                message = flag? "Successfully Deleted" : "Delete Failed";
                return flag;
            }
        }

    }
}