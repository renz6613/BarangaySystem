using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BarangaySystem.Data;
using BarangaySystem.Models.ActivityModels;
using BarangaySystem.Utilities;
using Microsoft.EntityFrameworkCore;

namespace BarangaySystem.Models.APIServices
{
    public class APIActivity
    {
        public async Task<(List<ActivityModel> list, int count)> GetActivity(int start, int pageLength, string key)
        {
            using(var db = new DbBarangayContext()){
                db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                if(key.IsEmpty()){
                    return await Get0();
                }

                return await GetSearch();

                async Task<(List<ActivityModel> _list, int _count)> Get0()
                {
                    var query = await db.ActivityLogs.ToListAsync();
                    int count = query.Count;
                    var rows  = query.OrderByDescending(x => x.ActivityDate).Skip(start).Take(pageLength);

                    var result = new List<ActivityModel>();
                    foreach (var item in rows)
                    {
                        result.Add(new ActivityModel{
                            Description = item.Description,
                            ActivityDate = item.ActivityDate.ToString()
                        });
                    }
                    return (result, count);
                }

                async Task<(List<ActivityModel> _list, int _count)> GetSearch()
                {
                    var query = await db.ActivityLogs.Where(x => x.Description.Contains(key) || x.ActivityDate.ToString().Contains(key)).ToListAsync();
                    int count = query.Count;
                    var rows  = query.OrderByDescending(x => x.ActivityDate).Skip(start).Take(pageLength);

                    var result = new List<ActivityModel>();
                    foreach (var item in rows)
                    {
                        result.Add(new ActivityModel{
                            Description = item.Description,
                            ActivityDate = item.ActivityDate.ToString()
                        });
                    }
                    return (result, count);
                }
            }
        }
    }
}