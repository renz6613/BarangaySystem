using System;
using System.Collections.Generic;
using System.Linq;
using BarangaySystem.Data;
using BarangaySystem.Models.BorrowItemModels;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BarangaySystem.Models.APIServices {
    public class APIBorrowItem {

        /// <summary>
        /// Add Persons Borrowed Item
        /// </summary>
        /// <param name="personModel">the person and Items data</param>
        /// <param name="addedBy">who Added the Data</param>
        /// <param name="message"></param>
        /// <returns>the Added person and Items</returns>
        public bool AddBorrow (BorrowItemModel borrowItemModel, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                var tm = db.Models.FirstOrDefault (x => x.Id == borrowItemModel.ItemName);
                if (tm.Quantity < borrowItemModel.ItemQuantity) {
                    message = $"Not enough stock";
                    return false;
                }
                if (borrowItemModel.ItemQuantity <= 0) {
                    message = "Invalid Input";
                    return false;
                }

                var em = new BorrowItem {
                    PersonBorrower = borrowItemModel.PersonBorrower,
                    ItemName = borrowItemModel.ItemName,
                    DateReserved = borrowItemModel.DateReserved,
                    ItemQuantity = borrowItemModel.ItemQuantity,
                    Status = "Reserved",
                    BorrowedTime = borrowItemModel.BTime

                };

                db.Entry (em).State = EntityState.Added;

                if (tm.Quantity >= em.ItemQuantity) {
                    tm.Quantity = tm.Quantity - em.ItemQuantity.GetValueOrDefault();

                }
                if (tm.Quantity == 0) {
                    tm.ItemStatus = 2;
                }

                db.Entry (tm).State = EntityState.Modified;

                var sta = db.Persons.FirstOrDefault(x => x.Id == borrowItemModel.PersonBorrower);

                var act = new ActivityLogs();
                act.Description = $"{sta.Fname} {sta.Mname} {sta.Lname} Reserved the item {tm.Name} prepared by {addedBy}";
                act.ActivityDate = DateTime.Now;
                db.Entry (act).State = EntityState.Added;
                
                var bp = new BorrowPayment();
                    bp.BusinessId = sta.Id;
                    bp.Name = sta.Fname;

                db.Entry (bp).State = EntityState.Added;

                bool flag = db.SaveChanges () > 0;
                message = flag? "Successfully Reserved!": "Reserving failed";
                return flag;
            }
        }

        public BorrowItemModel GetBorrow (int id) {
            using (var db = new DbBarangayContext ()) {
                var em = db.Persons.FirstOrDefault (x => x.Id == id);
                var person = new BorrowItemModel {
                    PersonBorrower = em.Id,
                    Key = em.Id.ToString ().EncryptString ()
                };
                return person;
            }
        }

        /// <summary>
        /// Get the Items Data for dropdown
        /// </summary>
        /// <value>Items Data</value>
        public IEnumerable<SelectOptions> ItemValues {
            get {
                using (var db = new DbBarangayContext ()) {
                    var model = new Data.Items ();
                    // var itembrand = new AssignedUserModel();
                    var rows = db.Items;
                    foreach (var item in rows) {
                        yield return new SelectOptions {
                            Value = item.Id,
                                Text = $"{item.Name} -> ({ item.Brand })"
                        };
                    }
                }
            }
        }

        public IEnumerable<SelectOptions> PersonValues {
            get {
                using (var db = new DbBarangayContext ()) {
                    // var model = new Data.Items ();
                    // var itembrand = new AssignedUserModel();
                    var rows = db.Persons;
                    foreach (var per in rows) {
                        yield return new SelectOptions {
                            Value = per.Id,
                            Text = $"{per.Fname} { per.Mname } {per.Lname}"
                        };
                    }
                }
            }
        }

        public List<BorrowItemViewModel> GetBorrowsData (int start, int pageSize, out int count) {
            using (var db = new DbBarangayContext ()) {
                // var query = db.BorrowItem;

                var query = from p in db.Persons
                join b in db.BorrowItem on p.Id equals b.PersonBorrower into gpPersonModels
                from personModel in gpPersonModels.DefaultIfEmpty ()

                join m in db.Models on personModel.ItemName equals m.Id into gpModelModels
                from modelModel in gpModelModels.DefaultIfEmpty ()

                join i in db.Items on modelModel.ItemId equals i.Id into gpItemModels
                from itemModel in gpItemModels.DefaultIfEmpty ()
                select new {
                    Id = personModel.Id,
                    Fname = p.Fname,
                    Mname = p.Mname,
                    Lname = p.Lname,
                    itemName = itemModel.Name,
                    itemBrand = itemModel.Brand,
                    itemMo = modelModel.Name,
                    Quantity = personModel.ItemQuantity,
                    Status = personModel.Status,
                    Reserve = personModel.DateReserved,
                    Release = personModel.DateReleased,
                    Dreturn = personModel.DateToReturn,
                    BorrowedTime = personModel.BorrowedTime

                };

                //You Stop here
                count = query.Count ();
                var rows = query.OrderByDescending (x => x.Id).Skip (start).Take (pageSize);
                var borrow = new List<BorrowItemViewModel> ();
                foreach (var em in rows) {

                    borrow.Add (new BorrowItemViewModel {
                            Id = em.Id.ToString(),
                            Key = em.Id.ToString().EncryptString(),
                            PersonBorrower = $"{em.Fname} {em.Mname} {em.Lname}",
                            ItemName = $"{em.itemName} {em.itemBrand} {em.itemMo}",
                            ItemQuantity = em.Quantity.ToString(),
                            BorrowStatus = em.Status,
                            DateReserved = $"{em.Reserve} {em.BorrowedTime}",
                            DateReleased = em.Release.ToString(),
                            DateToReturn = em.Dreturn.ToString()


                            // ModelItem = $"{em.Name} {em.Brand} {em.ModelItems}",
                            // Quantity = em.Quantity.HasValue ? em.Quantity.ToString() : string.Empty
                    });
                }
                return borrow;
            }
        }

        /// <summary>
        /// Search the items
        /// </summary>
        /// <param name="key">Any keywords to be search</param>
        /// <param name="start">Row of an offset to get</param>
        /// <param name="pageSize">Number of rows to fetch</param>
        /// <param name="count">Total of records</param>
        /// <returns>Returns the data fetched</returns>
        public List<BorrowItemViewModel> SearchBorrowsData (string key, int start, int pageSize, out int count) {
            using (var db = new DbBarangayContext ()) {
                var query = from p in db.Persons
                join b in db.BorrowItem on p.Id equals b.PersonBorrower into gpPersonModels
                from personModel in gpPersonModels.DefaultIfEmpty ()

                join m in db.Models on personModel.ItemName equals m.Id into gpModelModels
                from modelModel in gpModelModels.DefaultIfEmpty ()

                join i in db.Items on modelModel.ItemId equals i.Id into gpItemModels
                from itemModel in gpItemModels.DefaultIfEmpty ()

                where p.Fname.Contains(key) || p.Mname.Contains(key) || p.Lname.Contains(key) || personModel.Status.Contains(key) || itemModel.Name.Contains(key) || itemModel.Brand.Contains(key) || modelModel.Name.Contains(key)
                select new {
                    per = p.Id,
                    Id = personModel.Id,
                    Fname = p.Fname,
                    Mname = p.Mname,
                    Lname = p.Lname,
                    itemName = itemModel.Name,
                    itemBrand = itemModel.Brand,
                    itemMo = modelModel.Name,
                    Quantity = personModel.ItemQuantity,
                    Status = personModel.Status,
                    Reserve = personModel.DateReserved,
                    Release = personModel.DateReleased,
                    Dreturn = personModel.DateToReturn,
                    BorrowedTime = personModel.BorrowedTime

                };
                count = query.Count ();
                var rows = query.OrderByDescending (x => x.per & x.Id).Take(pageSize).Skip(start);
                var borrow = new List<BorrowItemViewModel> ();
                foreach (var em in rows) {
                    borrow.Add (new BorrowItemViewModel {
                            Id = em.Id.ToString(),
                            PersonBorrower = $"{em.Fname} {em.Mname} {em.Lname}",
                            ItemName = $"{em.itemName} {em.itemBrand} {em.itemMo}",
                            ItemQuantity = em.Quantity.ToString(),
                            BorrowStatus = em.Status,
                            DateReserved = $"{em.Reserve} ({em.BorrowedTime})",
                            DateReleased = em.Release.ToString(),
                            DateToReturn = em.Dreturn.ToString()
                    });
                }
                return borrow;
            }
        }

        public List<BorrowItemViewModel> GetBorrowData (int id,int start, int pageSize, out int count) {
            using (var db = new DbBarangayContext ()) {
                var bp = db.BorrowItem.FirstOrDefault(x => x.Id == id);
                var per = db.Persons.FirstOrDefault(x => x.Id == bp.PersonBorrower);
                // var query = db.BorrowItem;

                var query = from p in db.Persons.Where(x => x.Id == per.Id)
                join b in db.BorrowItem on p.Id equals b.PersonBorrower into gpPersonModels
                from personModel in gpPersonModels.DefaultIfEmpty ()

                join m in db.Models on personModel.ItemName equals m.Id into gpModelModels
                from modelModel in gpModelModels.DefaultIfEmpty ()

                join i in db.Items on modelModel.ItemId equals i.Id into gpItemModels
                from itemModel in gpItemModels.DefaultIfEmpty ()
                select new {
                    Id = p.Id,
                    itemName = itemModel.Name,
                    itemBrand = itemModel.Brand,
                    itemMo = modelModel.Name,
                    Quantity = personModel.ItemQuantity

                };

                //You Stop here
                count = query.Count ();
                var rows = query.OrderByDescending (x => x.Id).Skip (start).Take (pageSize);
                var borrow = new List<BorrowItemViewModel> ();
                foreach (var em in rows) {

                    borrow.Add (new BorrowItemViewModel {
                            Id = em.Id.ToString(),
                            Key = em.Id.ToString().EncryptString(),
                            ItemName = $"{em.itemName} {em.itemBrand} {em.itemMo}",
                            ItemQuantity = em.Quantity.ToString()


                            // ModelItem = $"{em.Name} {em.Brand} {em.ModelItems}",
                            // Quantity = em.Quantity.HasValue ? em.Quantity.ToString() : string.Empty
                    });
                }
                return borrow;
            }
        }

    }
}