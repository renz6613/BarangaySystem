using System;
using System.Collections.Generic;
using System.Linq;
using BarangaySystem.Data;
using BarangaySystem.Models.ItemModels;
using BarangaySystem.Models.ModelModels;
using BarangaySystem.Utilities;
using Microsoft.EntityFrameworkCore;

namespace BarangaySystem.Models.APIServices
{
    public class APIModels
    {
        public bool Add(ModelModel model, string LoggedUser, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                if(db.Models.Where(x => x.Name == model.Name && x.ItemId == model.ItemId).Any ()) {
                    message = "Oppss..!! This Item is already exist..!!";
                    return false;
                }
                var em  = new Data.Models {
                    ItemId = model.ItemId,
                    Name = model.Name,
                    Price = model.Price.ConvertToDecimal(),
                    Quantity = model.Quantity,
                    AddedBy = LoggedUser,
                    DateAdded = DateTime.Now,
                    ItemStatus = 1,
                    Total = model.Price.ConvertToDecimal() * model.Quantity
                };
                db.Entry (em).State = EntityState.Added;


                    var act = new ActivityLogs();
                    act.Description = $"The Item {model.Name} was added by {LoggedUser}";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;

                bool flag = db.SaveChanges () > 0;

                message = flag ? "Successfully Added!" : "Insert failed";
                return flag;
            }
        }

         /// <summary>
        /// Updates the items data
        /// </summary>
        /// <param name="itemModel">the Models data</param>
        /// <param name="addedBy">who updates the Models</param>
        /// <param name="message"></param>
        /// <returns>the updated item</returns>
        public bool Update(ModelModel modelModel, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                if(db.Models.Where (x => x.Name == modelModel.Name).Any () && db.Models.Where (x => x.ItemId == modelModel.ItemId).Any ()
                 && db.Models.Where (x => x.Quantity == modelModel.Quantity + x.Quantity).Any ()) {
                    message = "Oppss..!! This Item is already exist..!!";
                    return false;
                }

                var em  = db.Models.FirstOrDefault(x => x.Id == modelModel.Id);
                    em.Name = modelModel.Name;
                    em.ItemId = modelModel.ItemId;
                    em.Price = modelModel.Price.ConvertToDecimal();
                    em.Quantity = modelModel.Quantity; // em.Quantity + modelModel.Quantity
                    em.AddedBy = addedBy;
                    em.DateModified = DateTime.Now;
                    em.ItemStatus = 1;
                    em.Total = modelModel.Price.ConvertToDecimal() * modelModel.Quantity;

                db.Entry (em).State = EntityState.Modified;

                    var act = new ActivityLogs();
                    act.Description = $"The Item {modelModel.Name} was Updated by {addedBy}";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;

                bool flag = db.SaveChanges () > 0;
                message = flag? "Successfully Updated!" : "Insert failed";
                return flag;
            }
        }


        public List<ModelViewModel> GetEquipmentData(int start , int pageSize, out int count) {
            using (var db= new DbBarangayContext ()){

                    var query = from m in db.Models 
                                join i in db.Items on m.ItemId equals i.Id into gpIteModels
                                from item in gpIteModels.DefaultIfEmpty()

                                join s in db.Status on m.ItemStatus equals s.Id into gpItemStatus
                                from Itemsta in gpItemStatus.DefaultIfEmpty()

                                select new
                                {
                                    m.Id,
                                    item.Name,
                                    item.Brand,
                                    Model = m.Name,
                                    Price = m.Price,
                                    Quantity = m.Quantity,
                                    AddedBy = m.AddedBy,
                                    DateAdded = m.DateAdded,
                                    ItemStatus = Itemsta.Name,
                                    DateModified = m.DateModified,
                                    Total = m.Price * m.Quantity
                                };
                    count = query.Count();
                    var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);
                    var items = new List<ModelViewModel> ();
                    foreach (var em in rows) {
                        string id = em.Id.ToString();
                        var total = em.Price * em.Quantity;
                        items.Add (new ModelViewModel {
                            Id = id,
                            Key = id.EncryptString(),
                            Name = em.Name,
                            Brand = em.Brand,
                            Model = em.Model,
                            Price = em.Price.ToString(),
                            Quantity = em.Quantity.ToString(),
                            ItemStatus = em.ItemStatus,
                            AddedBy = em.AddedBy,
                            Total = total.ToString(),
                            DateAdded = em.DateAdded.ToString("MMM dd, yyyy hh:mm:ss tt"),
                            DateModified = em.DateModified.HasValue ? em.DateModified.Value.ToString("MMM dd, yyyy hh:mm:ss tt") : string.Empty
                        });
                    }
                    return items;
                }
        }

        public List<ModelViewModel> GetInventoryData(int start , int pageSize, out int count) {
            using (var db= new DbBarangayContext ()){

                    var query = from m in db.Models 
                                join i in db.Items on m.ItemId equals i.Id into gpIteModels
                                from item in gpIteModels.DefaultIfEmpty()

                                join s in db.Status on m.ItemStatus equals s.Id into gpItemStatus
                                from Itemsta in gpItemStatus.DefaultIfEmpty()

                                join b in db.BorrowItem on m.Id equals b.ItemName into gpBorrow
                                from BorrowItem in gpBorrow.DefaultIfEmpty()

                                select new
                                {
                                    m.Id,
                                    item.Name,
                                    item.Brand,
                                    Model = m.Name,
                                    Price = m.Price,
                                    Quantity = m.Quantity,
                                    Total = m.Price * m.Quantity,
                                    BorrowQuantity = BorrowItem.ItemQuantity
                                };
                    count = query.Count();
                    var rows = query.OrderByDescending(x => x.Id).Skip(start).Take(pageSize);
                    var items = new List<ModelViewModel> ();
                    foreach (var em in rows) {
                        string id = em.Id.ToString();
                        var total = em.Price * em.Quantity;
                        items.Add (new ModelViewModel {
                            Id = id,
                            Key = id.EncryptString(),
                            Name = $"{em.Name} {em.Brand} {em.Model}",
                            Price = em.Price.ToString(),
                            Quantity = em.Quantity.ToString(),
                            Total = total.ToString(),
                            BorrowedItem = em.BorrowQuantity.ToString()
                        });
                    }
                    return items;
                }
        }

        /// <summary>
        /// Search the items
        /// </summary>
        /// <param name="key">Any keywords to be search</param>
        /// <param name="start">Row of an offset to get</param>
        /// <param name="pageSize">Number of rows to fetch</param>
        /// <param name="count">Total of records</param>
        /// <returns>Returns the data fetched</returns>
        public List<ModelViewModel> SearchEquipmentData(string key, int start , int pageSize, out int count) {
            using (var db= new DbBarangayContext ()){
                    var query = from m in db.Models 
                                join i in db.Items on m.ItemId equals i.Id into gpIteModels
                                from item in gpIteModels.DefaultIfEmpty()

                                join s in db.Status on m.ItemStatus equals s.Id into gpItemStatus
                                from Itemsta in gpItemStatus.DefaultIfEmpty()

                                where Itemsta.Name.StartsWith(key) || item.Name.StartsWith(key) || item.Brand.StartsWith(key) || m.Name.StartsWith(key)
                                select new
                                {
                                    m.Id,
                                    item.Name,
                                    item.Brand,
                                    Model = m.Name,
                                    Price = m.Price,
                                    Quantity = m.Quantity,
                                    m.AddedBy,
                                    m.DateAdded,
                                    ItemStatus = Itemsta.Name,
                                    m.DateModified

                                };
                    count = query.Count();
                    var rows = query.OrderByDescending(x => x.Id).Take(pageSize).Skip(start);
                    var items = new List<ModelViewModel> ();
                    foreach (var em in rows) {
                        string id = em.Id.ToString();
                        items.Add (new ModelViewModel {
                            Id = id,
                            Key = id.EncryptString(),
                            Name = em.Name,
                            Brand = em.Brand,
                            Model = em.Model,
                            Price = em.Price.ToString(),
                            Quantity = em.Quantity.ToString(),
                            ItemStatus = em.ItemStatus,
                            AddedBy = em.AddedBy,
                            DateAdded = em.DateAdded.ToString("MMM dd, yyyy hh:mm:ss tt"),
                            DateModified = em.DateModified.HasValue ? em.DateModified.Value.ToString("MMM dd, yyyy hh:mm:ss tt") : string.Empty
                        });
                    }
                    return items;
                }
        }

         public bool Delete (int id, string addedBy, out string message) {
            using(var db = new DbBarangayContext ()) {
                message = string.Empty;
                var em = db.Models.FirstOrDefault (x => x.Id == id);
                db.Entry (em).State = EntityState.Deleted;

                    var act = new ActivityLogs();
                    act.Description = $"The Item {em.Name} was removed by {addedBy}";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;

                bool flag = db.SaveChanges () > 0;
                message = flag? "Successfully Deleted" : "Delete Failed";
                return flag;
            }
        }
        
        /// <summary>
        /// Get the Band of an item by BrandName
        /// </summary>
        /// <param name="id">id of an item</param>
        /// <returns>the items data</returns>
        public string GetBrandNameById(int ItemId){
            using (var db= new DbBarangayContext ()){
                return db.Items.FirstOrDefault(x => x.Id == ItemId).Brand;
            }
        }

        /// <summary>
        /// Get the Models Data for Edit
        /// </summary>
        /// <param name="id">Models Id</param>
        /// <returns>the Models Data</returns>
        public ModelModel GetModel (int id) {
            using (var db = new DbBarangayContext())
            {
                var em = db.Models.FirstOrDefault ( x => x.Id == id);
                var api = new APIModels();
                var mOdels = new ModelModel
                {
                    ItemId = em.ItemId,
                    Id = em.Id,
                    Key = em.Id.ToString().EncryptString(),
                    BrandName = api.GetBrandNameById(int.Parse(em.ItemId.ToString())),
                    Name = em.Name,
                    Price = em.Price.ToString(),
                    Quantity = em.Quantity.GetValueOrDefault()
                };
                return mOdels;
            }
        }
    }
}
