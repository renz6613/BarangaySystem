using System;
using System.Collections.Generic;
using System.Linq;
using BarangaySystem.Data;
using BarangaySystem.Models.PersonModels;
using BarangaySystem.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BarangaySystem.Models.APIServices {
    public class APIPersons {
        public bool Add (PersonModel personModel, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                if (db.Persons.Where (x => x.Fname == personModel.Fname && x.Lname == personModel.Lname && x.Mname == personModel.Mname).Any ()) {
                    message = "Oppss..!! This Person is already exist..!!";
                    return false;
                }
                if (db.Persons.Where (x => x.Email == personModel.Email || x.ContactNumber == personModel.ContactNumber).Any ()) {
                    message = "Oppss..!! This Conctact is already Used by another Person..!!";
                    return false;
                }

                var em = new Persons {
                    Fname = personModel.Fname,
                    Mname = personModel.Mname,
                    Lname = personModel.Lname,
                    Gender = personModel.Gender,
                    Age = personModel.Age,
                    Address = personModel.Address,
                    Email = personModel.Email,
                    ContactNumber = personModel.ContactNumber
                };
                db.Entry (em).State = EntityState.Added;

                    var act = new ActivityLogs();
                    act.Description = $"The Borrower {personModel.Fname}{personModel.Mname}{personModel.Lname}  was Recorded by {addedBy}";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;

                bool flag = db.SaveChanges () > 0;

                message = flag ? "Successfully Added!" : "Insert failed";
                return flag;
            }
        }

        /// <summary>
        /// Updates the Persons data
        /// </summary>
        /// <param name="personModel">the person data</param>
        /// <param name="addedBy">who updates the person</param>
        /// <param name="message"></param>
        /// <returns>the updated person</returns>
        public bool Update (PersonModel personModel, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                if (db.Persons.Where (x => x.Fname == personModel.Fname && x.Lname == personModel.Lname && x.Mname == personModel.Mname).Any ()) {
                    message = "Oppss..!! This Person is already exist..!!";
                    return false;
                }

                var em = db.Persons.FirstOrDefault (x => x.Id == personModel.Id);
                em.Fname = personModel.Fname;
                em.Mname = personModel.Mname;
                em.Lname = personModel.Lname;
                em.Gender = personModel.Gender;
                em.Age = personModel.Age;
                em.Address = personModel.Address;
                em.Email = personModel.Email;
                em.ContactNumber = personModel.ContactNumber;

                db.Entry (em).State = EntityState.Modified;

                    var act = new ActivityLogs();
                    act.Description = $"The Borrower {personModel.Fname}{personModel.Mname}{personModel.Lname}  was Updated by {addedBy}";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;

                bool flag = db.SaveChanges () > 0;
                message = flag? "Successfully Updated!": "Insert failed";
                return flag;
            }
        }

        /// <summary>
        /// Get the Items Data for Edit
        /// </summary>
        /// <param name="id">items Id</param>
        /// <returns>the Items Data</returns>
        public PersonModel GetPerson (int id) {
            using (var db = new DbBarangayContext ()) {
                var em = db.Persons.FirstOrDefault (x => x.Id == id);
                var person = new PersonModel {
                    Id = em.Id,
                    Key = em.Id.ToString ().EncryptString (),
                    Fname = em.Fname,
                    Mname = em.Mname,
                    Lname = em.Lname,
                    Gender = em.Gender,
                    Age = em.Age.Value,
                    Address = em.Address,
                    Email = em.Email,
                    ContactNumber = em.ContactNumber
                };
                return person;
            }
        }
         public PersonBorrowSlip GetPersonBorrow (int id, string issuedBy) {
            using (var db = new DbBarangayContext ()) {
                var br = db.BorrowItem.FirstOrDefault(x => x.Id == id);
                var em = db.Persons.FirstOrDefault (x => x.Id == br.PersonBorrower);
                var person = new PersonBorrowSlip {
                    Id = em.Id.ToString(),
                    Key = em.Id.ToString ().EncryptString (),
                    Fname = em.Fname,
                    Mname = em.Mname,
                    Lname = em.Lname,
                    Gender = em.Gender,
                    Age = em.Age.Value.ToString(),
                    Address = em.Address,
                    Email = em.Email,
                    ContactNumber = em.ContactNumber,
                    DateReleased = br.DateReleased.ToString(),
                    DateReturn = br.DateToReturn.ToString(),
                    DateReserved = br.DateReserved,
                    IssuedBy = issuedBy,
                    BorrowedTime = br.BorrowedTime
                    
                };
                return person;
            }
        }


        /// <summary>
        /// Get the Models Data for drop down
        /// </summary>
        /// <param name="itemId">id of the model</param>
        public IEnumerable<SelectOptions> GetModelValues (long itemId) {
            using (var db = new DbBarangayContext ()) {
                var rows = db.Models.Where (x => x.ItemId == itemId && x.ItemStatus == 1 && x.Quantity != 0);
                foreach (var m0del in rows) {
                    yield return new SelectOptions {
                        Value = m0del.Id,
                            Text = m0del.Name
                    };
                }
            }
        }

        public List<PersonViewModel> GetPersonsData (int start, int pageSize, out int count) {
            using (var db = new DbBarangayContext ()) {
                var query = db.Persons;

                // var query = from p in db.Persons
                // join b in db.BorrowItem on p.Id equals b.PersonBorrower into gpPersonModels
                // from personModel in gpPersonModels.DefaultIfEmpty ()

                // join m in db.Models on personModel.ItemName equals m.Id into gpModelModels
                // from modelModel in gpModelModels.DefaultIfEmpty ()

                // join i in db.Items on modelModel.ItemId equals i.Id into gpItemModels
                // from itemModel in gpItemModels.DefaultIfEmpty ()

                count = query.Count ();
                var rows = query.OrderByDescending (x => x.Id).Skip (start).Take (pageSize);
                var person = new List<PersonViewModel> ();
                foreach (var em in rows) {
                    string id = em.Id.ToString ();
                    person.Add (new PersonViewModel {
                        Id = id,
                            Key = id.EncryptString (),
                            Fname = em.Fname,
                            Mname = em.Mname,
                            Lname = em.Lname,
                            Age = em.Age.ToString(),
                            Gender = em.Gender,
                            Email = em.Email,
                            Address = em.Address,
                            ContactNumber = em.ContactNumber
                    });
                }
                return person;
            }
        }

        /// <summary>
        /// Search the items
        /// </summary>
        /// <param name="key">Any keywords to be search</param>
        /// <param name="start">Row of an offset to get</param>
        /// <param name="pageSize">Number of rows to fetch</param>
        /// <param name="count">Total of records</param>
        /// <returns>Returns the data fetched</returns>
        public List<PersonViewModel> SearchPersonsData (string key, int start, int pageSize, out int count) {
            using (var db = new DbBarangayContext ()) {
                var query = db.Persons;
                count = query.Count ();
                var rows = query.OrderByDescending (x => x.Id).Take (pageSize).Skip (start);
                var person = new List<PersonViewModel> ();
                foreach (var em in rows) {
                    string id = em.Id.ToString ();
                    person.Add (new PersonViewModel {
                            Id = id,
                            Fname = em.Fname,
                            Mname = em.Mname,
                            Lname = em.Lname,
                            Age = em.Age.ToString(),
                            Gender = em.Gender,
                            Email = em.Email,
                            Address = em.Address,
                            ContactNumber = em.ContactNumber
                    });
                }
                return person;
            }
        }

        public bool Delete (int id, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;

                if (!db.BorrowItem.Any ()) {
                    var em = db.Persons.FirstOrDefault (x => x.Id == id);
                    db.Entry (em).State = EntityState.Deleted;

                    var act = new ActivityLogs();
                    act.Description = $"The borrower {em.Fname} {em.Mname} {em.Lname}  was Removed by {addedBy}";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;
                }
                if (db.BorrowItem.Any ()) {
                    var em = db.Persons.FirstOrDefault (x => x.Id == id);
                    var borrow = db.BorrowItem.FirstOrDefault (x => x.PersonBorrower == id);

                    var tara = db.BorrowItem.FirstOrDefault (x => x.Id == borrow.Id);
                    db.Entry (tara).State = EntityState.Deleted;

                    var item = db.Models.FirstOrDefault (x => x.Id == tara.ItemName);
                    item.Quantity = item.Quantity + 1;
                    db.Entry (item).State = EntityState.Modified;

                    var act = new ActivityLogs();
                    act.Description = $"The item {item.Name} from borrower {em.Fname} {em.Mname} {em.Lname}  was Removed by {addedBy}";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;

                    db.Entry (em).State = EntityState.Deleted;
                }

                bool flag = db.SaveChanges () > 0;
                message = flag? "Successfully Deleted": "Delete Failed";
                return flag;
            }
        }

        public bool ItemRelease (int id, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                if (!db.BorrowItem.Any ()) {
                    return false;
                }

                    var bar = db.BorrowItem.FirstOrDefault(x => x.Id == id);
                    bar.DateReleased = DateTime.Now;
                    bar.Status = "Released";
                    db.Entry (bar).State = EntityState.Modified;

                    var stat = db.Persons.FirstOrDefault (x => x.Id == bar.PersonBorrower);

                    var item = db.Models.FirstOrDefault(x => x.Id == bar.ItemName);

                    var act = new ActivityLogs();
                    act.Description = $"{addedBy} released the item to {stat.Fname} {stat.Mname} {stat.Lname}";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;

                bool flag = db.SaveChanges () > 0;
                message = flag? "Successfully Released": "Release Failed";
                return flag;
            }
        }

        public bool ItemReturn (int id, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                if (!db.BorrowItem.Any ()) {
                    return false;
                }
                
                var bar = db.BorrowItem.FirstOrDefault(x => x.Id == id);
                if(bar.Status == "Returned"){
                    return false;
                }
                    bar.DateToReturn = DateTime.Now;
                    bar.Status = "Returned";

                var item = db.Models.FirstOrDefault (x => x.Id == bar.ItemName);
                item.Quantity = item.Quantity + bar.ItemQuantity.GetValueOrDefault();
                item.ItemStatus = 1;
                db.Entry (item).State = EntityState.Modified;

                var stat = db.Persons.FirstOrDefault(x => x.Id == bar.PersonBorrower);

                    var act = new ActivityLogs();
                    act.Description = $"{stat.Fname} {stat.Mname} {stat.Lname} return the item {item.Name} received by {addedBy} ";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;

                    db.Entry (bar).State = EntityState.Modified;

                    // db.Entry (bar).State = EntityState.Deleted;

                bool flag = db.SaveChanges () > 0;
                message = flag? "Successfully Returned": "Returning Failed";
                return flag;
            }
        }
    }
}