using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BarangaySystem.Data;
using BarangaySystem.Models.BPmodels;
using BarangaySystem.Models.PaymentModels;
using BarangaySystem.Utilities;
using Microsoft.EntityFrameworkCore;

namespace BarangaySystem.Models.APIServices
{
    public class APIPayment
    {
        public async Task<(List<PaymentModel> list, int count)> GetPayment(int start, int pageLength, string key)
        {
            using(var db = new DbBarangayContext()){
                db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                if(key.IsEmpty()){
                    return await Get0();
                }

                return await GetSearch();

                async Task<(List<PaymentModel> _list, int _count)> Get0()
                {
                    var query = await db.BusinessPermit.ToListAsync();
                    int count = query.Count;
                    var rows  = query.OrderByDescending(x => x.DateReleased).Skip(start).Take(pageLength);

                    var result = new List<PaymentModel>();
                    foreach (var item in rows)
                    { 
                        var bs = db.BusinessPermit.FirstOrDefault(x => x.Id == item.Id);
                        var b = db.BusinessPermit.FirstOrDefault(x => x.Name == bs.Name);
                        result.Add(new PaymentModel{
                            Name = b.Name,
                            TotalTax = b.TotalTax,
                            PermitFee = b.PermitFee,
                            PermitFee1 = b.PermitFee1,
                            PermitFee2 = b.PermitFee2,
                            PermitFee3 = b.PermitFee3,
                            Php = b.Php.ToString(),
                            Php1 = b.Php1.ToString(),
                            Php2 = b.Php2.ToString(),
                            Php3 = b.Php3.ToString()
                        });
                    }
                    return (result, count);
                }

                async Task<(List<PaymentModel> _list, int _count)> GetSearch()
                {
                    var query = await db.BusinessPermit.Where(x => x.Name.Contains(key) || x.BusinessName.Contains(key)).ToListAsync();
                    int count = query.Count;
                    var rows  = query.OrderByDescending(x => x.DateReleased).Skip(start).Take(pageLength);

                    var result = new List<PaymentModel>();
                    foreach (var item in rows)
                    {
                        result.Add(new PaymentModel{
                            Name = item.Name,
                            TotalTax = item.TotalTax,
                            PermitFee = item.PermitFee,
                            PermitFee1 = item.PermitFee1,
                            PermitFee2 = item.PermitFee2,
                            PermitFee3 = item.PermitFee3,
                            Php = item.Php.ToString(),
                            Php1 = item.Php1.ToString(),
                            Php2 = item.Php2.ToString(),
                            Php3 = item.Php3.ToString()
                        });
                    }
                    return (result, count);
                }
            }
        }
    }
}