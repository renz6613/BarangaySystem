using System;
using System.Collections.Generic;
using System.Linq;
using BarangaySystem.Data;
using BarangaySystem.Models.BlotterModels;
using BarangaySystem.Utilities;
using Microsoft.EntityFrameworkCore;

namespace BarangaySystem.Models.APIServices {
    public class APIBlotter {
        public bool Add (BlotterModel blotterModel, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                if (db.Blotter.Where (x => x.Name == blotterModel.Name && x.NameOfRespondent == blotterModel.NameOfRespondent).Any ()) {
                    message = "Oppss..!! This Person is already exist..!!";
                    return false;
                }

                var em = new Blotter {
                    Name = blotterModel.Name,
                    Age = blotterModel.Age,
                    Address = blotterModel.Address,
                    NameOfRespondent = blotterModel.NameOfRespondent,
                    RespondentAddress = blotterModel.RespondentAddress,
                    Facts = blotterModel.Facts,
                    RespondentAge = blotterModel.RespondentAge,
                    Purpose = blotterModel.Purpose,
                    BpsoonDuty = blotterModel.BpsoonDuty,
                    DateRecorded = DateTime.Now

                };
                db.Entry (em).State = EntityState.Added;

                var act = new ActivityLogs();
                act.Description = $"{addedBy} record a blotter from {blotterModel.Name}";
                act.ActivityDate = DateTime.Now;

                db.Entry (act).State = EntityState.Added;
                bool flag = db.SaveChanges () > 0;

                message = flag ? "Successfully Added!" : "Insert failed";
                return flag;
            }
        }

        /// <summary>
        /// Updates the Persons data
        /// </summary>
        /// <param name="blotterModel">the person data</param>
        /// <param name="addedBy">who updates the person</param>
        /// <param name="message"></param>
        /// <returns>the updated person</returns>
        public bool Update (BlotterModel blotterModel, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                if (db.Blotter.Where (x => x.Name == blotterModel.Name && x.NameOfRespondent == blotterModel.NameOfRespondent 
                && x.Address == blotterModel.Address && x.RespondentAddress == blotterModel.RespondentAddress 
                && x.Purpose == blotterModel.Purpose && x.BpsoonDuty == blotterModel.BpsoonDuty).Any ()) {
                    message = "Oppss..!! This Person is already exist..!!";
                    return false;
                }

                var em = db.Blotter.FirstOrDefault (x => x.Id == blotterModel.Id);
                em.Name = blotterModel.Name;
                em.Age = blotterModel.Age;
                em.Address = blotterModel.Address;
                em.NameOfRespondent = blotterModel.NameOfRespondent;
                em.RespondentAddress = blotterModel.RespondentAddress;
                em.Facts = blotterModel.Facts;
                em.RespondentAge = blotterModel.RespondentAge;
                em.Purpose = blotterModel.Purpose;
                em.BpsoonDuty = blotterModel.BpsoonDuty;
                em.DateRecorded = DateTime.Now;

                db.Entry (em).State = EntityState.Modified;
                
                var act = new ActivityLogs();
                act.Description = $"{addedBy} Updated the record of blotter from {blotterModel.Name}";
                act.ActivityDate = DateTime.Now;

                db.Entry (act).State = EntityState.Added;
                bool flag = db.SaveChanges () > 0;
                message = flag? "Successfully Updated!": "Insert failed";
                return flag;
            }
        }

        /// <summary>
        /// Get the Complaints Data for Edit
        /// </summary>
        /// <param name="id">Complaints Id</param>
        /// <returns>the Complaints Data</returns>
        public BlotterModel GetBlotter (int id) {
            using (var db = new DbBarangayContext ()) {
                var em = db.Blotter.FirstOrDefault (x => x.Id == id);
                var blotter = new BlotterModel {
                    Id = em.Id,
                    Key = em.Id.ToString ().EncryptString (),
                    Name = em.Name,
                    Age = em.Age,
                    Address = em.Address,
                    NameOfRespondent = em.NameOfRespondent,
                    RespondentAddress = em.RespondentAddress,
                    Facts = em.Facts,
                    RespondentAge = em.RespondentAge.Value,
                    Purpose = em.Purpose,
                    BpsoonDuty = em.BpsoonDuty,
                    DateRecorded = em.DateRecorded.ToString()
                };
                return blotter;
            }
        }
        public BlotterModel GetDesc (int id) {
            using (var db = new DbBarangayContext ()) {
                var em = db.Blotter.FirstOrDefault (x => x.Id == id);
                var blotter = new BlotterModel {
                    Id = em.Id,
                    Key = em.Id.ToString ().EncryptString (),
                    Name = em.Name,
                    Facts = em.Facts
                };
                return blotter;
            }
        }

        public List<BlotterViewModel> GetBlotterData (int start, int pageSize, out int count) {
            using (var db = new DbBarangayContext ()) {

                var query = db.Blotter;
                count = query.Count ();
                var rows = query.OrderByDescending (x => x.Id).Skip (start).Take (pageSize);
                var blotter = new List<BlotterViewModel> ();
                foreach (var em in rows) {
                    string id = em.Id.ToString ();
                    blotter.Add (new BlotterViewModel {
                        Id = id,
                            Key = id.EncryptString (),
                            Name = em.Name,
                            Age = em.Age.ToString (),
                            Address = em.Address,
                            NameOfRespondent = em.NameOfRespondent,
                            RespondentAddress = em.RespondentAddress,
                            Facts = em.Facts,
                            RespondentAge = em.RespondentAge.ToString (),
                            Purpose = em.Purpose,
                            BpsoonDuty = em.BpsoonDuty,
                            DateRecorded = em.DateRecorded.ToString()
                    });
                }
                return blotter;
            }
        }

        /// <summary>
        /// Search the items
        /// </summary>
        /// <param name="key">Any keywords to be search</param>
        /// <param name="start">Row of an offset to get</param>
        /// <param name="pageSize">Number of rows to fetch</param>
        /// <param name="count">Total of records</param>
        /// <returns>Returns the data fetched</returns>
        public List<BlotterViewModel> SearchBlotterData (string key, int start, int pageSize, out int count) {
            using (var db = new DbBarangayContext ()) {
                var query = db.Blotter.Where (x => x.Name.StartsWith (key) ||
                    x.Address.StartsWith (key) ||
                    x.NameOfRespondent.StartsWith (key));
                count = query.Count ();
                var rows = query.OrderByDescending (x => x.Id).Take (pageSize).Skip (start);
                var blotter = new List<BlotterViewModel> ();
                foreach (var em in rows) {
                    string id = em.Id.ToString ();
                    blotter.Add (new BlotterViewModel {
                        Id = id,
                            Name = em.Name,
                            Age = em.Age.ToString(),
                            Address = em.Address,
                            NameOfRespondent = em.NameOfRespondent,
                            RespondentAddress = em.RespondentAddress,
                            Facts = em.Facts,
                            RespondentAge = em.RespondentAge.Value.ToString(),
                            Purpose = em.Purpose,
                            BpsoonDuty = em.BpsoonDuty,
                            DateRecorded = em.DateRecorded.ToString()
                    });
                }
                return blotter;
            }
        }

        public bool Delete (int id, string addedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                var em = db.Blotter.FirstOrDefault (x => x.Id == id);
                db.Entry (em).State = EntityState.Deleted;
                
                var act = new ActivityLogs();
                act.Description = $"{addedBy} removed the blotter record from {em.Name}";
                act.ActivityDate = DateTime.Now;

                db.Entry (act).State = EntityState.Added;
                bool flag = db.SaveChanges () > 0;
                message = flag? "Successfully Deleted": "Delete Failed";
                return flag;
            }
        }
    }
}