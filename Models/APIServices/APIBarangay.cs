using System;
using System.Collections.Generic;
using System.Linq;
using BarangaySystem.Data;
using BarangaySystem.Models.PermitModels;
using BarangaySystem.Utilities;
using Microsoft.EntityFrameworkCore;

namespace BarangaySystem.Models.APIServices
{
    public class APIBarangay
    {
        public bool Add(BarangayModel barangayModel, string ReleasedBy, out string message) {
            using (var db = new DbBarangayContext ()) {
                message = string.Empty;
                if(db.BarangayClearance.Where(x => x.Name == barangayModel.Name 
                                              && x.DateOfBirth == barangayModel.DateOfBirth).Any ()) {
                    message = "This Person is already exist..!!";
                    return false;
                }


                var em  = new BarangayClearance {
                    Name = barangayModel.Name,
                    Address = barangayModel.Address,
                    DateOfBirth = barangayModel.DateOfBirth,
                    Purpose = barangayModel.Purpose,
                    IssuedDate = barangayModel.IssuedDate,
                    ReleasedBy = ReleasedBy,
                    DateRequested = DateTime.Now
                };
                db.Entry (em).State = EntityState.Added;

                var act = new ActivityLogs();
                act.Description = $"{ReleasedBy} Released the Barangay Clearance of {barangayModel.Name}";
                act.ActivityDate = DateTime.Now;
                db.Entry (act).State = EntityState.Added;
                
                bool flag = db.SaveChanges () > 0;

                message = flag ? "Successfully Added!" : "Insert failed";
                return flag;
            }
        }
        public List<BarangayViewModel> GetBarangayDatas (int start, int pageSize, out int count) {
            using (var db = new DbBarangayContext ()) {

                var query = db.BarangayClearance;
                count = query.Count ();
                var rows = query.OrderByDescending (x => x.Id).Skip (start).Take (pageSize);
                var barangay = new List<BarangayViewModel> ();
                foreach (var em in rows) {
                    string id = em.Id.ToString ();
                    barangay.Add (new BarangayViewModel {
                        Id = id,
                            Key = id.EncryptString (),
                            Name = em.Name,
                            Address = em.Address,
                            DateOfBirth = em.DateOfBirth.ToString("MMM dd, yyyy"),
                            Purpose = em.Purpose,
                            IssuedDate = em.IssuedDate,
                            ReleasedBy = em.ReleasedBy,
                            DateRequested = em.DateRequested.ToString("MMM dd, yyyy"),
                    });
                }
                return barangay;
            }
        }
        public List<BarangayViewModel> SearchBarangayData (string key, int start, int pageSize, out int count) {
            using (var db = new DbBarangayContext ()) {
                var query = db.BarangayClearance.Where (x => x.Name.Contains (key) ||
                    x.Address.Contains (key) ||
                    x.DateRequested.ToString().Contains (key));
                count = query.Count ();
                var rows = query.OrderByDescending (x => x.Id).Take (pageSize).Skip (start);
                var barangay = new List<BarangayViewModel> ();
                foreach (var em in rows) {
                    string id = em.Id.ToString ();
                    barangay.Add (new BarangayViewModel {
                        Id = id,
                            Key = id.EncryptString (),
                            Name = em.Name,
                            Address = em.Address,
                            DateOfBirth = em.DateOfBirth.ToString("MMM dd, yyyy"),
                            Purpose = em.Purpose,
                            IssuedDate = em.IssuedDate,
                            ReleasedBy = em.ReleasedBy,
                            DateRequested = em.DateRequested.ToString("MMM dd, yyyy hh:mm:ss tt"),
                    });
                }
                return barangay;
            }
        }

        public BarangayViewModel GetPrintBarangay (int id) {
            using (var db = new DbBarangayContext ()) {
                var em = db.BarangayClearance.FirstOrDefault ( x => x.Id == id);
                var printB = new BarangayViewModel {
                    Name = em.Name,
                    DateOfBirth = em.DateOfBirth.ToString("MMM dd, yyyy"),
                    Address = em.Address,
                    Purpose = em.Purpose,
                    DateReleased = DateTime.Now.ToString("MMM dd, yyyy"),
                    IssuedDate = em.IssuedDate
                };
                return printB;
            }
        }

        public IEnumerable<SelectOptions> PersonValues {
            get {
                using (var db = new DbBarangayContext ()) {
                    // var model = new Data.Items ();
                    // var itembrand = new AssignedUserModel();
                    var rows = db.Persons;
                    foreach (var per in rows) {
                        yield return new SelectOptions {
                            Value = per.Id,
                            Text = $"{per.Fname} { per.Mname } {per.Lname}"
                        };
                    }
                }
            }
        }

        public IEnumerable<SelectOptions> GetAddressValues (long itemId) {
            using (var db = new DbBarangayContext ()) {
                var rows = db.Persons.Where (x => x.Id == itemId);
                foreach (var m0del in rows) {
                    yield return new SelectOptions {
                        Value = m0del.Id,
                        Text = m0del.Address
                    };
                }
            }
        }
    }
}