using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BarangaySystem.Data;
using BarangaySystem.Models.BPmodels;
using BarangaySystem.Models.PermitModels;
using BarangaySystem.Utilities;
using Microsoft.EntityFrameworkCore;

namespace BarangaySystem.Models.APIServices
{
    public class APIBusiness
    {
        public async Task<(bool status, string message)> SavePermit(BusinessModel model){
            using(var db = new DbBarangayContext()){
                try{
                    var result = await Save();
                    return (result.s, result.m);
                }catch(Exception ex){
                    return (false, ex.InnerException.Message);
                }
                async Task<(bool s, string m)> Save(){
                    if(db.BusinessPermit.Any(x => x.Name == model.Name &&
                    x.BusinessName == model.BusinessName &&
                    x.BusinessAddress == model.BusinessAddress &&
                    x.Bbpno == model.Bbpno &&
                    x.DateRequested == model.DateRequested)){
                        return(false, "Oops!? This business permit is already added!");
                    }
                    var newPermit = new BusinessPermit
                    {
                        Name = model.Name,
                        BusinessName = model.BusinessName,
                        BusinessAddress = model.BusinessAddress,
                        Bbpno = model.Bbpno,
                        DateRequested = model.DateRequested,
                        RequestType = model.TypeOfRequest,
                        AssessedBy = model.AssessedBy,
                        DateReleased = DateTime.Now,
                        
                        PermitFee = model.PermitFee,
                        Php = model.Php,
                        ReleasedBy = model.ReleasedBy
                    };
                    decimal totalTax = 0.00m;
                    totalTax = model.Php;
                    if(!model.PermitFee1.IsEmpty() && model.Php1.HasValue){
                        newPermit.PermitFee1 = model.PermitFee1;
                        newPermit.Php1 = model.Php1.Value;
                        totalTax += model.Php1.Value;
                    }
                    if(!model.PermitFee2.IsEmpty() && model.Php2.HasValue){
                        newPermit.PermitFee2 = model.PermitFee2;
                        newPermit.Php2 = model.Php2.Value;
                        totalTax += model.Php2.Value;
                    }
                    if(!model.PermitFee3.IsEmpty() && model.Php3.HasValue){
                        newPermit.PermitFee3 = model.PermitFee3;
                        newPermit.Php3 = model.Php3.Value;
                        totalTax += model.Php3.Value;
                    }
                    newPermit.TotalTax = totalTax;
                    db.Entry(newPermit).State = EntityState.Added;

                    var act = new ActivityLogs();
                    act.Description = $"{model.ReleasedBy} released a Business permit to {model.Name}";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;

                    var bp = new BorrowPayment();
                    bp.Name = model.Name;
                    db.Entry (bp).State = EntityState.Added;

                    await db.SaveChangesAsync();
                    return (true, "Successfully added!");
                }
            }
        }

        public async Task<(bool status, string message)> UpdatePermit(BusinessModel model){
            using(var db = new DbBarangayContext()){
                try{
                    var result = await Update();
                    return (result.s, result.m);
                }catch(Exception ex){
                    return (false, ex.InnerException.Message);
                }
                async Task<(bool s, string m)> Update(){
                    int id = model.Id.DecryptString().ValidateNumber();
                    if(db.BusinessPermit.Any(x => x.Id != id && x.Name == model.Name &&
                    x.BusinessName == model.BusinessName &&
                    x.BusinessAddress == model.BusinessAddress &&
                    x.Bbpno == model.Bbpno &&
                    x.DateRequested == model.DateRequested)){
                        return(false, "Oops!? This business permit is already added!");
                    }

                    var editPermit = await db.BusinessPermit.FindAsync(id);

                    editPermit.Name = model.Name;
                    editPermit.BusinessName = model.BusinessName;
                    editPermit.BusinessAddress = model.BusinessAddress;
                    editPermit.Bbpno = model.Bbpno;
                    editPermit.DateRequested = model.DateRequested;
                    editPermit.RequestType = model.TypeOfRequest;
                    editPermit.AssessedBy = model.AssessedBy;
                    editPermit.DateReleased = DateTime.Now;
                    
                    editPermit.PermitFee = model.PermitFee;
                    editPermit.Php = model.Php;
                    editPermit.ReleasedBy = model.ReleasedBy;
                    decimal totalTax = 0.00m;

                    totalTax = model.Php;
                    if(!model.PermitFee1.IsEmpty() && model.Php1.HasValue){
                        editPermit.PermitFee1 = model.PermitFee1;
                        editPermit.Php1 = model.Php1.Value;
                        totalTax += model.Php1.Value;
                    }
                    if(!model.PermitFee2.IsEmpty() && model.Php2.HasValue){
                        editPermit.PermitFee2 = model.PermitFee2;
                        editPermit.Php2 = model.Php2.Value;
                        totalTax += model.Php2.Value;
                    }
                    if(!model.PermitFee3.IsEmpty() && model.Php3.HasValue){
                        editPermit.PermitFee3 = model.PermitFee3;
                        editPermit.Php3 = model.Php3.Value;
                        totalTax += model.Php3.Value;
                    }
                    editPermit.TotalTax = totalTax;
                    db.Entry(editPermit).State = EntityState.Modified;

                    var act = new ActivityLogs();
                    act.Description = $"{model.ReleasedBy} Updates the business permit for {model.Name}";
                    act.ActivityDate = DateTime.Now;
                    db.Entry (act).State = EntityState.Added;

                    var bp = new BorrowPayment();
                    bp.Name = model.Name;
                    db.Entry (bp).State = EntityState.Added;

                    await db.SaveChangesAsync();
                    return (true, "Successfully upodated!");
                }
            }
        }

        public async Task<BusinessModel> EditBusiness(int id)
        {
            using(var db = new DbBarangayContext()){
                var model = await db.BusinessPermit.FindAsync(id);
                return new BusinessModel
                {
                    Id = id.ToString().EncryptString(),
                    Name = model.Name,
                    BusinessName = model.BusinessName,
                    BusinessAddress = model.BusinessAddress,
                    Bbpno = model.Bbpno,
                    DateRequested = model.DateRequested,
                    TypeOfRequest = model.RequestType,
                    AssessedBy = model.AssessedBy,
                    PermitFee = model.PermitFee,
                    Php = model.Php,

                    PermitFee1 = model.PermitFee1,
                    Php1 = model.Php1,
                    PermitFee2 = model.PermitFee2,
                    Php2 = model.Php2,
                    PermitFee3 = model.PermitFee3
                };
            }
        }

        public async Task<(List<BusinessViewModel> list, int count)> GetBusiness(int start, int pageLength, string key)
        {
            using(var db = new DbBarangayContext()){
                db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                if(key.IsEmpty()){
                    return await Get0();
                }

                return await GetSearch();


                
                async Task<(List<BusinessViewModel> _list, int _count)> Get0()
                {
                    var query = await db.BusinessPermit.ToListAsync();
                    int count = query.Count;
                    var rows  = query.OrderByDescending(x => x.DateReleased).Skip(start).Take(pageLength);

                    var result = new List<BusinessViewModel>();
                    foreach (var item in rows)
                    {
                        result.Add(new BusinessViewModel{
                            Key = item.Id.ToString().EncryptString(),
                            Date = item.DateRequested.ToString("MMM dd, yyyy"),
                            Name = item.Name,
                            Business = item.BusinessName,
                            TypeOfRequest = item.RequestType,
                            Address = item.BusinessAddress,
                            TotalTax = item.TotalTax.ToString("N2"),
                            ReleasedBy = item.ReleasedBy
                        });
                    }
                    return (result, count);
                }

                async Task<(List<BusinessViewModel> _list, int _count)> GetSearch()
                {
                    var query = await db.BusinessPermit.Where(x => x.Name.Contains(key) || x.BusinessName.Contains(key)).ToListAsync();
                    int count = query.Count;
                    var rows  = query.OrderByDescending(x => x.DateReleased).Skip(start).Take(pageLength);

                    var result = new List<BusinessViewModel>();
                    foreach (var item in rows)
                    {
                        result.Add(new BusinessViewModel{
                            Key = item.Id.ToString().EncryptString(),
                            Date = item.DateRequested.ToString("MMM dd, yyyy"),
                            Name = item.Name,
                            Business = item.BusinessName,
                            Address = item.BusinessAddress,
                            TotalTax = item.TotalTax.ToString("N2"),
                            ReleasedBy = item.ReleasedBy
                        });
                    }
                    return (result, count);
                }
            }
        }


        public BusinessPrint GetPrintBusiness (int id) {
            using (var db = new DbBarangayContext ()) {
                var em = db.BusinessPermit.FirstOrDefault ( x => x.Id == id);
                var printB = new BusinessPrint {
                    Name = em.Name,
                    BusinessName = em.BusinessName,
                    ReleasedBy = em.ReleasedBy,
                    DateReleased = DateTime.Now.ToString("MMM dd, yyyy"),
                    DateRequested = em.DateRequested.ToString("MMM dd, yyyy"),
                    BusinessAddress = em.BusinessAddress,
                    BBPno = em.Bbpno.ToString(),
                    TypeOfRequest = em.RequestType,
                    AssessedBy = em.AssessedBy,
                    PermitFee = em.PermitFee,
                    PHP = em.Php.ToString(),
                    PermitFee1 = em.PermitFee1,
                    PHP1 = em.Php1.ToString(),
                    PermitFee2 = em.PermitFee2,
                    PHP2 = em.Php2.ToString(),
                    PermitFee3 = em.PermitFee3,
                    PHP3 = em.Php3.ToString(),
                    TotalTax = em.TotalTax.ToString()
                };
                return printB;
            }
        }

        public BusinessPrint GetOR (int id) {
            using (var db = new DbBarangayContext ()) {
                var em = db.BusinessPermit.FirstOrDefault ( x => x.Id == id);
                var printB = new BusinessPrint {
                    Name = em.Name,
                    BusinessName = em.BusinessName,
                    BusinessAddress = em.BusinessAddress,
                    BBPno = em.Bbpno.ToString(),
                    AssessedBy = em.AssessedBy,
                    PermitFee = em.PermitFee,
                    PHP = em.Php.ToString(),
                    PermitFee1 = em.PermitFee1,
                    PHP1 = em.Php1.ToString(),
                    PermitFee2 = em.PermitFee2,
                    PHP2 = em.Php2.ToString(),
                    PermitFee3 = em.PermitFee3,
                    PHP3 = em.Php3.ToString(),
                    TotalTax = em.TotalTax.ToString(),
                    DateReleased = DateTime.Now.ToString()
                };
                return printB;
            }
        }

        public BusinessModel GetBusinesses () {
            using (var db = new DbBarangayContext ()) {
                int id =int.Parse("");
                var em = db.BusinessPermit.FirstOrDefault (x => x.Id == id);
                var person = new BusinessModel {
                    Id = em.Id.ToString(),
                    Name = em.Name,
                    PermitFee = em.PermitFee,
                    PermitFee1 = em.PermitFee1,
                    PermitFee2 = em.PermitFee2,
                    PermitFee3 = em.PermitFee3,
                    Php = em.Php,
                    Php1 = em.Php1,
                    Php2 = em.Php2,
                    Php3 = em.Php3,
                    TotalTax = em.TotalTax
                };
                return person;
            }
        }
    }
}