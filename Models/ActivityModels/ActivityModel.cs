namespace BarangaySystem.Models.ActivityModels
{
    public class ActivityModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string ActivityDate { get; set; }
    }
}