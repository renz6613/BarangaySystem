namespace BarangaySystem.Models.PermitFeeModels
{
    public class PermitFeeModel
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string PermitFee1 { get; set; }
        public decimal Php { get; set; }
    }
}