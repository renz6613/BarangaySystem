using System;
using System.ComponentModel.DataAnnotations;

namespace BarangaySystem.Models.ModelModels
{
    public class ModelModel
    {
        public int Id { get; set; }
        public string Key { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Name { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Price { get; set; }
        public string AddedBy { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateModified { get; set; }
        public int ItemId { get; set; }
        public int ItemStatus { get; set; }
        public string BrandName { get; set; }
        [Required (ErrorMessage="This field is required")]
        public int Quantity { get; set; }
    }

    public class ModelViewModel
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string TypeOfItem { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Price { get; set; }
        public string AddedBy { get; set; }
        public string DateAdded { get; set; }
        public string DateModified { get; set; }
        public string ItemStatus { get; set; }
        public string BrandName { get; set; }
        public string Quantity { get; set; }
        public string Total { get; set; }
        public string BorrowedItem { get; set; }
    }
}