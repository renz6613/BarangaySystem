using System.ComponentModel.DataAnnotations;

namespace BarangaySystem.Models.FacilityModels
{
    public class FacilityModel
    {
        public int Id { get; set; }
        public string Key { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Name { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Description { get; set; }
    }
    public class FacilityViewModel
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}