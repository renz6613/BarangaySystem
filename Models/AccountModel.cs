namespace BarangaySystem.Models
{
    public class AccountModel
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string ContactNumber { get; set; }
        public string RoleName { get; set; }
    }
}