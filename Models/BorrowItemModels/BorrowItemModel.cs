using System;
using System.ComponentModel.DataAnnotations;

namespace BarangaySystem.Models.BorrowItemModels
{
    public class BorrowItemModel
    {
        public int Id { get; set; }
        public string Key { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string DateReserved { get; set; }
        public DateTime DateReleased { get; set; }
        [Required (ErrorMessage="This field is required")]
        public int ItemName { get; set; }
        [Required (ErrorMessage="This field is required")]
        public int PersonBorrower { get; set; }
        public DateTime DateToReturn { get; set; }
        [Required (ErrorMessage="This field is required")]
        public int ItemQuantity { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string BTime { get; set; }

    }
    public class BorrowItemViewModel
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string DateReserved { get; set; }
        public string DateReleased { get; set; }
        public string ItemName { get; set; }
        public string PersonBorrower { get; set; }
        public string DateToReturn { get; set; }
        public string ItemQuantity { get; set; }
        public string BorrowStatus { get; set; }
        public string BTime { get; set; }
        
    }
}