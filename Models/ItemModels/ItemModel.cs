using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BarangaySystem.Models.ItemModels
{

    public class ItemModel
    {
        public int Id { get; set; }
        public string Key { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string Name { get; set; }
        [Required (ErrorMessage="This field is required")]
        public string  Brand  { get; set; }
        public string AddedBy { get; set; }
    }
    public class ItemViewModel
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string  Brand  { get; set; }
        public string Models { get; set; }
        public string AddedBy { get; set; }
        public string DateAdded { get; set; }
        public string DateModified { get; set; }
    }
}