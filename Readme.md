# Barangay System Web Application using **ASP.NET Core with Identity** Thesis in *Asian College of Technology* Cebu Main Campus

>TODOS://
> * [ ] Model, View and Functions for
>   * [ ] Equipments
>   * [ ] Facilities
>   * [ ] Permits
>   * [ ] Clearance
>   * [ ] Resisdents Info
>   * [ ] Payments

---

> **Updates 0.0.1**
> * Create database
> * Create Identity models
>   * ApplicationUser
>   * ApplicationRole
> * Identity Settings in *```Startup.cs```*
>   * User
>   * Password
> * Migrates
>   * Tables
>   * Database update

>**Updates 0.0.2**
>* Boilerplate of layouts
>* Create System Tables in DB
>* Model classes
>* Action page in Controllers
>* Login and Logout
>* Register User
>* Edit and Update User
>* View User