using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using BarangaySystem.Models.IdentityModels;
using BarangaySystem.Utilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BarangaySystem.Services
{
    public class EmailSender : IEmailSender
    {
        public EmailSettings _emailSettings { get; set; }
        public ILogger<EmailSender> _logger { get; set; }
        public EmailSender(IOptions<EmailSettings> emailsettings, ILogger<EmailSender> logger)
        {
            this._emailSettings = emailsettings.Value;
            this._logger = logger;
        }

        public async Task Execute(string email, string message)
        {
            try{
                _logger.LogInformation("Setting up message...");
                var sb = new System.Text.StringBuilder();
                sb.Append($"<h2>{nameof(BarangaySystem)}</h2>{Environment.NewLine}");
                sb.Append($"{_emailSettings.HtmlMessagePrefix}{message}{_emailSettings.HtmlMessageSuffix}{Environment.NewLine}");
                sb.Append(_emailSettings.HtmlFooterMessage);

                string formattedMessage = sb.ToString();
                var mailMessage = new MailMessage(_emailSettings.FromEmail, email)
                {
                    Subject = "Reset Password",
                    Body = formattedMessage,
                    IsBodyHtml = true,
                    Priority = MailPriority.High
                };
                using(var smtp = new SmtpClient())
                {
                    // smtp.EnableSsl = true;
                    smtp.Host = _emailSettings.PrimaryDomain;
                    smtp.Port = _emailSettings.PrimaryPort;
                    smtp.Credentials = new NetworkCredential{
                        UserName = _emailSettings.UsernameEmail,
                        Password = _emailSettings.UsernamePassword.DecryptString()
                    };
                    _logger.LogInformation("Sending...");
                    await smtp.SendMailAsync(mailMessage);
                }
            }
            catch(Exception ex){
                _logger.LogError(ex,ex.Message,"Message won't send!");
            }
        }
        
        public Task SendEmailAsync(string email, string message)
        {
            Execute(email, message).Wait();
            return Task.FromResult(0);
        }

        Task IEmailSender.SendEmailAsync(string email, string message)
        {
            Execute(email, message).Wait();
            return Task.FromResult(0);
        }
    }

}