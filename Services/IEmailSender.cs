using System.Threading.Tasks;

namespace BarangaySystem.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string message);
    }

}