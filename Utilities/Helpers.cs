using System;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace BarangaySystem.Utilities
{

        public class CipherService {
        private readonly IDataProtectionProvider _dataProtectionProvider;
        
        private const string Key = "my-[)3f@uLt▬K3y";

        public CipherService (IDataProtectionProvider dataProtectionProvider) {
            _dataProtectionProvider = dataProtectionProvider;
        }

        public string Encrypt (string input) {
            var protector = _dataProtectionProvider.CreateProtector (Key);
            return protector.Protect (input);
        }
        
        public string Decrypt (string cipherText) {
            var protector = _dataProtectionProvider.CreateProtector (Key);
            return protector.Unprotect (cipherText);
        }
    }
    internal static class StringHelpers
    {
         /// <summary>
        /// Custom string value if it is empty.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsEmpty (this string value) {
            return (string.IsNullOrEmpty (value) || string.IsNullOrWhiteSpace (value)) || string.Equals (value, string.Empty, StringComparison.Ordinal);
        }

        public static string EncryptString (this string value) {

            //use data protection services
            var SCollection = new ServiceCollection ();

            //add protection services
            SCollection.AddDataProtection ();
            var SerPro = SCollection.BuildServiceProvider ();

            // create an instance of classfile using 'CreateInstance' method
            var instance = ActivatorUtilities.CreateInstance<CipherService> (SerPro);
            return instance.Encrypt (value);
        }

        public static string DecryptString (this string value) {
            try {
                //use data protection services
                var SCollection = new ServiceCollection ();

                //add protection services
                SCollection.AddDataProtection ();
                var SerPro = SCollection.BuildServiceProvider ();

                // create an instance of classfile using 'CreateInstance' method
                var instance = ActivatorUtilities.CreateInstance<CipherService> (SerPro);
                return instance.Decrypt (value);
            } catch {
                return string.Empty;
            }
        }

        public static decimal ConvertToDecimal(this string value){
            decimal result = 0.00m;
            string removedCommas = value.Replace(",", string.Empty);
            decimal.TryParse(removedCommas, out result);
            return result;
        }
        public static int ValidateNumber(this string value)
        {
            var result = 0;
            int.TryParse(value, out result);
            return result;
        }
          public static bool IsValidKey(this string value){
            return !value.DecryptString().IsEqualsTo(string.Empty);
        }
        /// <summary>
        /// Any string value will converted to upper then, compare them into case sensitive mode.
        /// </summary>
        /// <param name="value">Current value.</param>
        /// <param name="compareValue">Parameter value to compare</param>
        /// <returns></returns>
        public static bool IsEqualsTo(this string value, string compareValue){
            value = value.ToUpper();
            compareValue = compareValue.ToUpper();
            return string.Equals (value,compareValue, StringComparison.Ordinal);
        }


        /// <summary>
        /// This method compares the current value id to protected value into ordinal case-sensitive.
        /// </summary>
        /// <param name="value1">The id value</param>
        /// <param name="value2">Protected id</param>
        /// <returns>True if two strings are equal.static Otherwise is false.</returns>
        public static bool Validate (this string value1, string value2) {
            return string.Equals (value1, value2.DecryptString (), StringComparison.Ordinal);
        }

        public static bool IsValid (this string value) {
            return !string.Equals (string.Empty, value, StringComparison.Ordinal);
        }

        public static int ConvertToNumber(this string value){
            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}