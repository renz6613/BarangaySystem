﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class Blotter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public string NameOfRespondent { get; set; }
        public string Facts { get; set; }
        public string RespondentAddress { get; set; }
        public int? RespondentAge { get; set; }
        public string Purpose { get; set; }
        public string BpsoonDuty { get; set; }
        public DateTime? DateRecorded { get; set; }
    }
}
