﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class PermitFee
    {
        public PermitFee()
        {
            BusinessPermit = new HashSet<BusinessPermit>();
        }

        public int Id { get; set; }
        public string PermitFee1 { get; set; }
        public decimal Php { get; set; }

        public ICollection<BusinessPermit> BusinessPermit { get; set; }
    }
}
