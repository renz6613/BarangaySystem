﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class Items
    {
        public Items()
        {
            Models = new HashSet<Models>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateModified { get; set; }

        public ICollection<Models> Models { get; set; }
    }
}
