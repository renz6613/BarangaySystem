﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class ReturnedItem
    {
        public ReturnedItem()
        {
            Payments = new HashSet<Payments>();
        }

        public int Id { get; set; }
        public DateTime DateReturned { get; set; }
        public int ItemReturned { get; set; }
        public int PersonName { get; set; }

        public Models ItemReturnedNavigation { get; set; }
        public Persons PersonNameNavigation { get; set; }
        public ICollection<Payments> Payments { get; set; }
    }
}
