﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class BorrowPayment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? BusinessId { get; set; }
        public int? ViewBname { get; set; }
    }
}
