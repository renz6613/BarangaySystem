﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class ActivityLogs
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime? ActivityDate { get; set; }
    }
}
