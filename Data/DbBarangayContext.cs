﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BarangaySystem.Data
{
    public partial class DbBarangayContext : DbContext
    {
        public virtual DbSet<ActivityLogs> ActivityLogs { get; set; }
        public virtual DbSet<BarangayClearance> BarangayClearance { get; set; }
        public virtual DbSet<Blotter> Blotter { get; set; }
        public virtual DbSet<BorrowItem> BorrowItem { get; set; }
        public virtual DbSet<BorrowPayment> BorrowPayment { get; set; }
        public virtual DbSet<BusinessPermit> BusinessPermit { get; set; }
        public virtual DbSet<Facility> Facility { get; set; }
        public virtual DbSet<Items> Items { get; set; }
        public virtual DbSet<Models> Models { get; set; }
        public virtual DbSet<Payments> Payments { get; set; }
        public virtual DbSet<Persons> Persons { get; set; }
        public virtual DbSet<ReturnedItem> ReturnedItem { get; set; }
        public virtual DbSet<Status> Status { get; set; }

        public static string GetConnectionString { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(GetConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActivityLogs>(entity =>
            {
                entity.Property(e => e.ActivityDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<BarangayClearance>(entity =>
            {
                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.DateRequested).HasColumnType("datetime");

                entity.Property(e => e.IssuedDate)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Purpose)
                    .IsRequired()
                    .HasMaxLength(80);

                entity.Property(e => e.ReleasedBy)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Blotter>(entity =>
            {
                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.BpsoonDuty)
                    .HasColumnName("BPSOonDuty")
                    .HasMaxLength(60);

                entity.Property(e => e.DateRecorded).HasColumnType("datetime");

                entity.Property(e => e.Facts)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(60);

                entity.Property(e => e.NameOfRespondent)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Purpose).HasMaxLength(100);

                entity.Property(e => e.RespondentAddress)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<BorrowItem>(entity =>
            {
                entity.Property(e => e.DateReleased).HasColumnType("datetime");

                entity.Property(e => e.DateReserved)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DateToReturn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.ItemNameNavigation)
                    .WithMany(p => p.BorrowItem)
                    .HasForeignKey(d => d.ItemName)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ItemName");

                entity.HasOne(d => d.PersonBorrowerNavigation)
                    .WithMany(p => p.BorrowItem)
                    .HasForeignKey(d => d.PersonBorrower)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PersonBorrower");
            });

            modelBuilder.Entity<BorrowPayment>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(60);
            });

            modelBuilder.Entity<BusinessPermit>(entity =>
            {
                entity.Property(e => e.AssessedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Bbpno).HasColumnName("BBPno");

                entity.Property(e => e.BusinessAddress)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.BusinessName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.DateReleased).HasColumnType("datetime");

                entity.Property(e => e.DateRequested).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PermitFee)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Php)
                    .HasColumnName("PHP")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Php1)
                    .HasColumnName("PHP1")
                    .HasColumnType("decimal(8, 2)");

                entity.Property(e => e.Php2)
                    .HasColumnName("PHP2")
                    .HasColumnType("decimal(8, 2)");

                entity.Property(e => e.Php3)
                    .HasColumnName("PHP3")
                    .HasColumnType("decimal(8, 2)");

                entity.Property(e => e.ReleasedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.RequestType)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.TotalTax).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<Facility>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(1000);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<Items>(entity =>
            {
                entity.Property(e => e.AddedBy).HasMaxLength(50);

                entity.Property(e => e.Brand)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateModified).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Models>(entity =>
            {
                entity.Property(e => e.AddedBy)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateModified).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Price).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.Models)
                    .HasForeignKey(d => d.ItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ItemId");

                entity.HasOne(d => d.ItemStatusNavigation)
                    .WithMany(p => p.Models)
                    .HasForeignKey(d => d.ItemStatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ItemStatus");
            });

            modelBuilder.Entity<Payments>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ReturnedIitemName).HasColumnName("ReturnedIItemName");

                entity.HasOne(d => d.BusinessPaymentNavigation)
                    .WithMany(p => p.Payments)
                    .HasForeignKey(d => d.BusinessPayment)
                    .HasConstraintName("FK_BusinessPayment");

                entity.HasOne(d => d.ReturnedIitemNameNavigation)
                    .WithMany(p => p.Payments)
                    .HasForeignKey(d => d.ReturnedIitemName)
                    .HasConstraintName("FK_ReturnedIItemName");
            });

            modelBuilder.Entity<Persons>(entity =>
            {
                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ContactNumber).HasMaxLength(50);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Fname)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Gender)
                    .IsRequired()
                    .HasColumnType("char(10)");

                entity.Property(e => e.Lname)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Mname)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ReturnedItem>(entity =>
            {
                entity.Property(e => e.DateReturned).HasColumnType("datetime");

                entity.HasOne(d => d.ItemReturnedNavigation)
                    .WithMany(p => p.ReturnedItem)
                    .HasForeignKey(d => d.ItemReturned)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ItemReturned");

                entity.HasOne(d => d.PersonNameNavigation)
                    .WithMany(p => p.ReturnedItem)
                    .HasForeignKey(d => d.PersonName)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PersonName");
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(50);
            });
        }
    }
}
