﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class BarangayClearance
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ReleasedBy { get; set; }
        public DateTime DateRequested { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }
        public string Purpose { get; set; }
        public string IssuedDate { get; set; }
    }
}
