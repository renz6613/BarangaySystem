﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class BorrowItem
    {
        public int Id { get; set; }
        public string DateReserved { get; set; }
        public DateTime? DateReleased { get; set; }
        public int ItemName { get; set; }
        public int PersonBorrower { get; set; }
        public DateTime? DateToReturn { get; set; }
        public int? ItemQuantity { get; set; }
        public string Status { get; set; }
        public string BorrowedTime { get; set; }

        public Models ItemNameNavigation { get; set; }
        public Persons PersonBorrowerNavigation { get; set; }
    }
}
