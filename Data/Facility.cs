﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class Facility
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
