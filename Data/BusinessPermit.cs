﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class BusinessPermit
    {
        public BusinessPermit()
        {
            Payments = new HashSet<Payments>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string BusinessName { get; set; }
        public string ReleasedBy { get; set; }
        public DateTime? DateReleased { get; set; }
        public DateTime DateRequested { get; set; }
        public string BusinessAddress { get; set; }
        public int Bbpno { get; set; }
        public string AssessedBy { get; set; }
        public string PermitFee { get; set; }
        public decimal Php { get; set; }
        public string PermitFee1 { get; set; }
        public decimal? Php1 { get; set; }
        public string PermitFee2 { get; set; }
        public decimal? Php2 { get; set; }
        public string PermitFee3 { get; set; }
        public decimal? Php3 { get; set; }
        public decimal TotalTax { get; set; }
        public string RequestType { get; set; }

        public ICollection<Payments> Payments { get; set; }
    }
}
