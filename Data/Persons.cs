﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class Persons
    {
        public Persons()
        {
            BorrowItem = new HashSet<BorrowItem>();
            ReturnedItem = new HashSet<ReturnedItem>();
        }

        public int Id { get; set; }
        public string Fname { get; set; }
        public string Mname { get; set; }
        public string Lname { get; set; }
        public int? Age { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }

        public ICollection<BorrowItem> BorrowItem { get; set; }
        public ICollection<ReturnedItem> ReturnedItem { get; set; }
    }
}
