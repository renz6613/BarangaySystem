﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class Status
    {
        public Status()
        {
            Models = new HashSet<Models>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Models> Models { get; set; }
    }
}
