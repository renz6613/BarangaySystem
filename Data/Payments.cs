﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class Payments
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public int? ReturnedIitemName { get; set; }
        public int? BusinessPayment { get; set; }

        public BusinessPermit BusinessPaymentNavigation { get; set; }
        public ReturnedItem ReturnedIitemNameNavigation { get; set; }
    }
}
