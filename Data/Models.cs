﻿using System;
using System.Collections.Generic;

namespace BarangaySystem.Data
{
    public partial class Models
    {
        public Models()
        {
            BorrowItem = new HashSet<BorrowItem>();
            ReturnedItem = new HashSet<ReturnedItem>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string AddedBy { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime? DateModified { get; set; }
        public int ItemId { get; set; }
        public int ItemStatus { get; set; }
        public int? Quantity { get; set; }
        public decimal? Total { get; set; }

        public Items Item { get; set; }
        public Status ItemStatusNavigation { get; set; }
        public ICollection<BorrowItem> BorrowItem { get; set; }
        public ICollection<ReturnedItem> ReturnedItem { get; set; }
    }
}
